
@extends('template.app')

@section('content')

<section id="betaald">

	<div class="container">
		<div class="row">




            <br />
            			@if($order->status === "1")
							<div class='alert alert-success betaald_msg'>
								<h3>DE BETALING IS GELUKT</h3>
							</div>					
						@else
							<div class='alert alert-danger betaald_msg'>
								<h4>DE BETALING IS MISLUKT</h4>
							</div>
						@endif

			<br />
					<div class="col col-md-2"></div>
					<div class="col col-md-8 betaald_gegevens body_blok text-center">
						
						<div class="col col-md-12 all_color categorie_headtext" style="border:none;">
							
							<div class="col col-md-5 col-md-offset-1 inherit_col">
								<h3>Gegevens:</h3>
							</div>
							<div class="col col-md-5 inherit_col">
								<h3>Adresgegevens:</h3>
							</div>

						</div>

						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-5">

								<p>{{ $order->voornaam}} 
								@if(empty($order->tussenvoegsel))@else{{ $order->tussenvoegsel}}@endif
									{{ $order->achternaam}}
								</p>



								<p> {{ $order->email}}</p>
								<p> {{ $order->telefoon}}</p>

							</div>
							<div class="col-md-5">

							@if(empty($order->bedrijfsnaam))
							@else
								<p>{{ $order->bedrijfsnaam}}</p>
								<p>{{ $order->BTW-nummer}}</p>
							@endif
								
								<p>{{ $order->straatnaam}} {{ $order->huisnummer}}
									@if(empty($order->toevoeging))@else{{ $order->toevoeging}}@endif</p>
								<p>{{ $order->postcode}}, {{ $order->plaatsnaam}}, {{$order->land}}</p>
							</div>

						</div>

					</div>
				</div>

		<div class="row">
			<div class="col col-md-1"></div>
			<div class="col col-md-10 text-center">
				




				<div class="row justify-content-md-center">

					@foreach($orderinfos as $orderinfo)

						<div class="col-md-6 text-center betaald_info catagorie_blok black_border">
							<div class="categorie_headtext">
								@if(empty($orderinfo->naam))
									Naam onbekend
								@else
									<h3>{{ $orderinfo->naam}}</h3>
								@endif
							</div>

							<div class="row">
								<div class="col-md-5">
									@if(empty($orderinfo->image))
										<a href="/product/{{$orderinfo->id}}"><img src="/img/noimage.png" class="betaald_img"></a>
									@else
										<a href="/product/{{$orderinfo->id}}"><img src="/img/noimage.png" class="betaald_img"></a>
		<!-- 								<img src="{{ $orderinfo->image }}" class="betaald_img"> -->
									@endif
								</div>
								<div class="col-md-7 betaald_text">

									@if(empty($orderinfo->naam))
										<p>Naam onbekend</p>
									@else
										<p>{{ $orderinfo->naam}}</p>
									@endif


										@if(empty($orderinfo->voertuig))
											<p>Voertuig onbekend</p>
										@else
											<p>{{ $orderinfo->voertuig }}</p>
										@endif

										@if(empty($orderinfo->prijs))
											Prijs onbekend
										@else
											<p> €{{ $orderinfo->prijs }}</p>
										@endif

								</div>
							</div>
						</div>

					@endforeach
				</div>

			<div class="row text-center">
				<div class=""



						@if(empty($order->order_id))
							orderid
						@else
							<h2>Order id: "{{ $order->order_id }}"</h2>
						@endif



						
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<table class="table table-striped winkelwagen">
							    <tbody>
							                <tr>
							                        <td>
							                            <h5>Subtotaal</h5>
							                        </td>
							                        <td>
							                            <p>€ {{ $order->prijs * (1/1.21) }}</p>
							                        </td>
							                   
							                </tr>
							                <tr>
							                        <td>
							                            <h5>Totaal inc btw. en verzendkosten</h5>
							                        </td>
							                        <td>
							                            <p>€{{ $order->prijs }}</p>
							                        </td>
							                    
							                </tr>

							            </tr>
							    </tbody>                   
							</table>
						</div>


				</div>
			</div>
		</div>
	</div>


	



</section>


@endsection
