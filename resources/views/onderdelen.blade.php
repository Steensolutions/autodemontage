@extends('template.app')

@section('content')


<section>

  <br />
        <div class="container">
            
            
<div class="col-sm-12 mobile_top blok_full_layout all_color">

        <ul class="top_blok">
            <li class="active"> <a class="top_tab tableft text-center" href="#tab1" data-toggle="tab">KENTEKEN / MERK & MODEL</a></li>                              
            <li> <a class="top_tab text-center" href="#tab2" data-toggle="tab">ARTIKELNUMMER</a></li>
                                  
            <li> <a class="top_tab text-center" href="#tab3" data-toggle="tab">VERSNELLINGSBAKCODE</a></li>

            <li> <a class="top_tab tabright text-center" href="#tab4" data-toggle="tab">MOTORCODE</a></li>
        </ul>



        <div class="tab-content bot_blok all_color">
  <div class="tab-pane fade in active tab_layout" id="tab1">

              <div class="col col-md-6 kenteken_pad">
                <div class="col col-md-12 kenteken">
                  <h3>Zoek op kenteken</h3>
                  <form action="/kenteken/" method="get">
                    <input type="text" name="kentekenplaat" class="text-center kentekenplaat">
                        <button value="Zoek" type=submit class=search_button>Zoek</button>

                        @if($errors->any())
                        <h5>{{$errors->first()}}</h5>
                        @endif

                  </form>
                </div>
              </div>

              <div class="col col-md-6">

                    <form action="/filter" method="get">
                      <div class="col col-md-12">
                        <div class="form-group">Merk
                          <select name="make" class="form-control" id="selectmake">
                            <option value="Selecteer een auto">Selecteer een merk.</option>
                            @foreach($makes as $make)
                            <option value="{{$make['name']}}">{{$make['name']}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col col-md-10">
                        <div class="form-group">
                          <select name="model" class="form-control" id="selectmodel">
                            <option>Selecteer een model.</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-2">
                          <button type="submit" class="search_button">Filter</button>
                      </div>
                    </form>

                    <form class="form-group" onsubmit="window.location = '/categorie/sub/' + search.value; return false;">
                      <div class="col col-md-12">Zoek op onderdeel per naam</div>
                      <div class="col col-md-10">
                        <div class="form-group">
                        <input id="search" class="search-query form-control" placeholder="Onderdeel"" type="search" name="search"></input>
                        </div>
                      </div>
                      <div class="col col-md-2">
                          <button type="submit" class="search_button">Zoek</button>
                      </div>
                    </form>     
              </div>
  </div>

 <div class="tab-pane fade tab_layout" id="tab2">
              <div class="col col-md-1"></div>
              <div class="col col-md-4">Zoek op onderdeel- / artikelnummer</div>
              <form onsubmit="window.location = '/product/' + search.value; return false;">
                <div class="col col-md-5">
                  <input id="search" class="search-query form-control" placeholder="nummer"" type="search" name="search"></input>
                </div>
                  <div class="col col-md-2"><button type="submit" class="search_button">Zoek</button></div>
              </form>
 </div>

 <div class="tab-pane fade tab_layout" id="tab3">


              <div class="col col-md-1"></div>
              <div class="col col-md-4">Zoek op versnellingsbakcode</div>
              <form onsubmit="window.location = '/versnellingsbakcode/' + search.value; return false;"> 
                <div class="col col-md-5">
                  <input id="search" class="search-query form-control" placeholder="code"" type="search" name="search"></input>
                </div>
                  <div class="col col-md-2"><button type="submit" class="search_button">Zoek</button></div>
              </form>
 </div>
  
 <div class="tab-pane fade tab_layout" id="tab4">


              <div class="col col-md-1"></div>
              <div class="col col-md-4">Zoek op motorcode</div>
              <form onsubmit="window.location = '/motorcode/' + search.value; return false;"> 
                <div class="col col-md-5">
                  <input id="search" class="search-query form-control" placeholder="code"" type="search" name="search"></input>
                </div>
                  <div class="col col-md-2"><button type="submit" class="search_button">Zoek</button></div>
              </form>
 </div>

</div>
</div>


    
  <div class="col-sm-12 storage_blok">

  <div class="">
  

     <h1> <i class="fa fa-wrench"></i> Beschikbare onderdelen</h1>
     <p> Maak een keuze uit een van de onderstaande subcategorieen</p>



  @foreach($categories as $categorie)
<a href="/categorie/{{ $categorie['categoryid'] }}">
<div class="part_blok text-center"> 
                    <div class="storage_text">
                         <p>{{ $categorie['categoryname'] }}</p>
                       </div>
            
<!--             <img class="storage_img" src="/images/icons/SVG/081-{{ App\Api::getImage($categorie['categoryname']) }}.svg"> -->
            <img class="storage_logo" src="/images/icons/SVG/logo.png">

                </div>
</a>
@endforeach

      </div>
    </div>

    </div>

  </div>
</div>
</div>

</section>





@endsection


@section('js')
     <script>

  $( "#selectmake" ).change(function() 
  {
    $.getJSON("/getmodel/"+ $(this).val() +"", function(jsonData){
        select = '<select name="position" class="form-control input-sm " required id="position" >';
          $.each(jsonData, function(i,data)
          {
               select +='<option value="'+data.name+'">'+data.name+'</option>';
           });
        select += '</select>';
        $("#selectmodel").html(select);
    });
  });
      </script>
@endsection