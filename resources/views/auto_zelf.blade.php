@extends('template.app')

@section('content')
<section>
<head>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
</head>


<div class="container">
  <div class="row">

    <br />

      <div class="col-sm-3 autos">
          <div class="left-sidebar categorie_layout">
              <div class="categorie_headtext all_color"> 
                <h3>AUTO'S</h3>
              </div>

              <div class="panel-group category-products body_blok" id="accordian">

@foreach($caroverzicht as $car)

          <div class="panel-heading">
            @if(empty($make['media']['images']['root']))
                <img src="/img/noimage.png" class="">
            @else
                <img src="{{ $car['media']['images']['root'] }}/large/0.jpg" class="" alt="">
            @endif  
                <a href="{{ route('showauto', $car['vehicleid'])}}">{{$car['makename']}} {{$car['modelname']}} </a>

          </div>

    @endforeach
              </div>
          </div>

    </div>
    <div class="col-sm-9 catagorie_blok black_border">


      <div class="row text-center">
        <div class="categorie_headtext all_color" style="border-radius: 0px;">
          <h2>{{$makes['makename']}} {{$makes['modelname']}}</h2>
          <p>
            @if(empty($makes['properties']['propertyvalue']['0']['values']['string']))
            @else
              {{$makes['properties']['propertyvalue']['0']['values']['string']}} 
            @endif


            @if(empty($makes['properties']['propertyvalue']['11']['values']['string']))
            @else
              {{$makes['properties']['propertyvalue']['11']['values']['string']}} 
            @endif


            @if($makes['state'] === "damaged")
              (Schade auto)
            @else
              (Sloop auto)
            @endif
          </p>
        </div>

        <div class="col-sm-6">

            @if(empty($makes['media']['images']['root']))
                <img src="/img/noimage.png" class="voorraad_img">
            @else
                <img src="/{{ $make['media']['images']['root'] }}/large/0.jpg" class="voorraad_img" alt="">
            @endif
        </div>
        <div class="col-sm-6">
            <p> RUIMTE VOOR PREVIEW ANDERE FOTOS VAN DE AUTO</p>
        </div>
      </div>

      <div class="row gegevens">
          <h1>Gegevens</h1> 

@foreach($gegevens as $gegeven)

  @if(empty($gegeven))

  @else
    <div class="col-sm-4">
        <div class="gegevens_blok">
          <h3>{{$api->getId($gegeven['id'])}}</h3>
            <p>{{$gegeven['values']['string']}}</p>
        </div>
    </div>
  @endif

@endforeach

      </div>

    </div>

  </div>
</div>
  


<script type="text/javascript">

</script>                  
        
@endsection
