@extends('template.app')

@section('content')
<section>
        <div class="container">

            <div class="row layout_reparation">
                
                <div class="col-sm-9">
<div class="col-md-12">
                    <div class="top_blok">
                       <div class="top_blok_left col-sm-12">

                        <b>Reparatie en onderhoud</b>
                                             <p>Heeft u onderdelen voor uw auto nodig maar bent u ook nog op zoek naar iemand, die ze voor u kan monteren? 

Dan bent u bij Autodemontage Veenendaal op het juiste adres.

Onderdelen, die bij ons gemonteerd worden krijgen standaard 3 maanden garantie. Dit geldt ook voor elektrische onderdelen.<br /><br />

Verder voeren wij ook reparaties en onderhoudsbeurten uit op afspraak.<br /><br />

Neem contact met ons op om een afspraak te maken voor een reparatie of onderhoudsbeurt.</p><br />
                           </div>
                       </div>

                   </div>
                   </div>
                </div>
    </section>
@endsection
