@extends('template.app')





@section('content')
<div class="container">
    


    <br />
    
    <div class="col-12 col-md-6">

    @if(session('message'))
    <div class='alert alert-success'>
        {{ session('message') }}
    </div>
    @endif


        <form class="form-horizontal" method="POST" action="/contact">
            {{ csrf_field() }} 
            <div class="form-group">
            <label for="Name">Volledige naam: </label>
            <input type="text" class="form-control" id="name" placeholder="Volledige (bedrijfs)naam" name="name" required>
        </div>

        <div class="form-group">
            <label for="email">E-mail-adres: </label>
            <input type="text" class="form-control" id="email" placeholder="Info@autodemontageveenendaal.nl" name="email" required>
        </div>

        <div class="form-group row">
            <label for="telefoonnummer">Telefoonnummer</label>
            <input id="telefoonnummer" type="text" class="form-control" name="telefoonnummer" placeholder="0318-307529" required>
        </div>

        <div class="form-group">
            <label for="message">Bericht: </label>
            <textarea type="text" class="form-control luna-message" id="message" placeholder="Typ hier uw bericht." name="message" rows="5" required></textarea>
        </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary" value="Send">Verstuur</button>
            </div>
        </form>
    </div>
    <div class="col-sm-6">
     <h3>Onze locatie</h3>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2455.9777215891536!2d5.5665190158455085!3d52.00728387972032!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c653c873c4ee8b%3A0xef37d53936e0ee6a!2sTurbinestraat+22A%2C+3903+LW+Veenendaal!5e0!3m2!1snl!2snl!4v1524430312037" width="100%" height="100%;" frameborder="0" style="border:0" allowfullscreen></iframe>

        <h3>Contactgegevens</h3>
                        <span>Autodemontage Veenendaal</span><br>
                        <span>Turbinestraat 22A</span><br>
                        <span>Veenendaal 3903LW </span><br><br>
                        <span>Info@autodemontageveenendaal.nl</span><br>
                        <span>0318-307529</span><br>

            <br />
    </div>
 </div> <!-- /container -->
@endsection