@extends('template.app')

@section('content')


<div class="container">
    <div class="col-sm-12 storage_blok">
        <div class="">
      

         <h1> <i class="fa fa-ambulance"></i> Beschikbare beschadigde auto's</h1>
         <p> Maak een keuze uit een van de onderstaande auto's</p>


    @foreach($makes as $make)

            @if($make['state'] === "damaged")

            <div class="part_blok voorraad text-center">

                @if(empty($make['media']['images']['root']))
                    <img src="/images/home/logo.png" class="voorraad_logo">
                @else
                    <img src="{{ $make['media']['images']['root'] }}/large/0.jpg" class="voorraad_img" alt="">
                @endif

                <div class="voorraad_text">
                    <a href="{{ route('showauto', $make['vehicleid'])}}">{{$make['makename']}} {{$make['modelname']}} </a>
                    <p>{{$make['properties']['propertyvalue'][0]['values']['string']}}</p>

                </div>
            </div>
            @else
            @endif

    @endforeach

        </div>
    </div>
</div>


@endsection

@section('js')
@endsection