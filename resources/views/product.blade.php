@extends('template.app')

@section('content')
<section>

<head>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

</head>
        <div class="container">
            <div class="row">
                
                @include('template.categoriebar')

                <div class="col-sm-9 padding-right">
                  <div class="row catagorie_blok black_border">


                                <div class="categorie_headtext all_color" style="border-radius: 0px;"> 
                                    <h3 class="text-center">
                                      @if(empty($products['result']['row']['partname']))
                                        Onbekend
                                      @else
                                        {{ $products['result']['row']['partname'] }}
                                      @endif
                                    </h3>
                                </div>

                      <div class="col-sm-12">

                          <div class="product-details"><!--product-details-->
                              <div class="col-sm-5 growDiv">
                                  <div id="similar-product" class="carousel slide" data-ride="carousel">
                                      
                                        <!-- Wrapper for slides -->
                                          <div class="carousel-inner">
                                              <div class="item active">
                                                <a href="">
                                                  @if(empty($products['result']['row']['media']['images']['root']))
                                                  <img src="img/noimage.png" class="growImage">
                                                  @else
                                                    <img class="growImage" src="{{ $products['result']['row']['media']['images']['root'] }}/medium/1.jpg" alt="my image"> 
                                                  @endif
                                                </a>
                                              </div>      

                                              @foreach($products['result']['row']['media']['images']['list']['stockrowimage'] as $product)
                                                 <div class="item">
                                                 @if(empty($products['result']['row']['media']['images']['list']['stockrowimage']))
                                                  
                                                      <img src="/img/noimage.png" class="growImage">
                                                  @else  
                                                    @if(empty($product['normal']))
                                                      <img src="/img/noimage.png" class="growImage">
                                                    @else
                                                      <a href=""><img class="growImage" src="{{ $products['result']['row']['media']['images']['root'] }}/{{ $product['normal'] }}" alt=""></a>
                                                        </div>
                                                    @endif
                                                  @endif

                                              @endforeach
                                                     
                                          </div>

                                        <!-- Controls -->
                                        <a class="left growImage item-control" href="#similar-product" data-slide="prev">
                                          <i class="fa fa-angle-left"></i>
                                        </a>
                                        <a class="right item-control" href="#similar-product" data-slide="next">
                                          <i class="fa fa-angle-right"></i>
                                        </a>
                                  </div>

                              </div>
                              <div class="col-sm-7">
                                
                                  <div class="product-information"><!--/product-information-->
                                    <div class="grow-float">
     
                                       @if(empty($products['result']['row']['partname']))
                                        <h2> onbekend</h2>
                                      @else
                                        <h2>{{ $products['result']['row']['partname'] }}</h2>
                                        <p>{{ $products['result']['row']['makename'] }}</p>
                                        <p>{{ $products['result']['row']['modeltypeid'] }}</p>
                                        <p>{{ $products['result']['row']['makename'] }} {{ $products['result']['row']['modelname'] }}</p>
                                      @endif
                                     
                                    
                                    </div>
                                    <div class="grow-float">


                                      <span>

                                          

                                            <span>€ {{ $products['result']['row']['properties']['partpropertyvalue'][0]['values']['string'] }}</span>

                                            @if($api->getQty( $products['result']['row']['partid']) == $products['result']['row']['quantity'] )

                                            @else 

                                              <a href="/add-to-cart/{{ $products['result']['row']['partid'] }}">
                                            <button type="button" class="btn btn-fefault cart" style="margin-top: 1%;">
                                                <i class="fa fa-shopping-cart"></i>
                                                Toevoegen aan winkelwagen
                                            </button></a>

                                            @endif
                                           </span>
                                      <p><b>In voorraad:</b> {{ $products['result']['row']['quantity'] }}</p>
                                      <p><b>Staat:</b> @if($products['result']['row']['partname'] === 'new')
                                      Nieuw 
                                      @else
                                      Gebruikt
                                      @endif 
                                                                      </p>
                                    </div>
                               

                                    </div>
                                  </div>

                                  <div class="row">
                                  @foreach($gegevens as $gegeven)
<!--                                     @if($gegeven['id'] === "14")
                                     @elseif($gegeven['id'] === "135")
                                     @elseif($gegeven['id'] === "18")
                                     @elseif($gegeven['id'] === "26")
                                     @elseif($gegeven['id'] === "149")
                                     @elseif($gegeven['id'] === "14")
                                     @elseif($gegeven['id'] === "27")
                                     @elseif($gegeven['id'] === "133")
                                     @elseif($gegeven['id'] === "146")
                                     @elseif($gegeven['id'] === "101")
                                     @elseif($gegeven['id'] === "110")
                                     @elseif($gegeven['id'] === "112")


                                      @else -->

                                      
                                        <div class="col-sm-6 gegevens_prod">
                                            <div class="gegevens_blok">
                                                     <h3>{{$api->getProd($gegeven['id'])}}</h3>
                                                <p>{{$gegeven['values']['string']}}</p>
                                            </div>
                                        </div>
                                      @endif

                                    @endforeach
                                    </div>



                                  </div><!--/product-information-->
                              </div>
                          </div><!--/product-details-->
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
  



<script type="text/javascript">

$(document).ready(function(){

    $('.growImage').mouseover(function(){

      $('.growImage').stop().animate({"position":"relative","width": "100%","margin":"0px","z-index":"1000"}, 400,'swing');
      $('.col-sm-5').stop().animate({"position":"relative","width": "80%","z-index":"1000"}, 400,'swing');
      $('.item-control i').stop().animate({"font-size" : "50px" , }, 400,'swing');
      $('.grow-float').stop().animate({"width":"40%"}, 400,'swing');
      $('.col-sm-7').stop().animate({"width" : "100%"}, 400,'swing');
    }).mouseout(function(){  

      $('.growImage').stop().animate({"width": "90%", "margin": "0% 5%"}, 200,'swing');
      $('.col-sm-5').stop().animate({"width": "41%" , "padding" : "0px 15px"}, 200,'swing');
      $('.item-control i').stop().animate({"font-size" : "30px" , "padding" : "0px"}, 400,'swing');
      $('.grow-float').stop().animate({"width" : "100%" }, 400,'swing');
    }).mouseout(function(){
      $('.col-sm-7').stop().animate({"width" : "58.333%"}, 400,'swing');
    }).mouseout(function(){  
    });;

});

</script>                  
        
@endsection
