                  
    <categoriebar id="categoriebar">
        <br />
                   <div class="col-sm-3">
                        <div class="left-sidebar categorie_layout">
                     
                        <div class="categorie_headtext"> 
                            <h3>CATEGORIE</h3>
                        </div>
                        <div class="panel-group category-products body_blok" id="accordian"><!--category-productsr-->
                            @foreach($categories as $categorie)

                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="@if(empty($name)) @else @if ($name === $categorie['categoryname']) selected @endif @endif" href="/categorie/{{ $categorie['categoryid'] }}">{{ $categorie['categoryname'] }}</a></h4>
                                </div>

                            @endforeach
                            </div>
                            
                        </div><!--/category-productsr-->

                </div>

    </categoriebar>