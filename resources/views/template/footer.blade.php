	
	<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
							<div class="col-md-6">
								<div class="companyinfo">
									<a href="/"><img class="logo" src="/images/home/logo.png" alt="" /></a>

								</div>
							</div>
								<div class="col-sm-6">
								<h3>Informatie</h3>
								<a href="/algemene-voorwaarden"><span>Algemene voorwaarden</span></a><br />
								<a href="/privacy"><span>Privacy</span></a><br />
								<a href="/disclaimer"><span>Disclaimer</span></a><br />

							</div>
							<div class="col-sm-12 footer_text">
								Wij zijn gespecialiseerd in het verkoop van verschillende gebruikte auto-onderdelen & accessoires. Zowel particulieren als bedrijven kunnen bij Autodemontage Veenendaal terecht.
							</div>
					</div>
					<div class="col-sm-3">
						<div class="footermap">
					<h3>Openingstijden</h3>
						<span>Maandag - Vrijdag: 09:00 - 17:00</span><br />
						<span>Zaterdag: 09:00 - 15:00</span><br />

                        </div>

                        <div class="footer_logos">
                        	<br />
                        	<img class="footer_logo" src="/img/iDeal_logo.png">
                        	<img class="footer_logo" src="/img/bancontact_logo.png">
                        	<img class="footer_visa" src="/img/visamastercard_logo.png">

                        </div>

					</div>		
					<div class="col-md-3">
						<h3>Contact</h3>
						<span>Autodemontage Veenendaal</span><br />
						<span>Turbinestraat 22A</span><br />
						<span>Veenendaal 3903LW </span><br /><br />
						<span>Info@autodemontageveenendaal.nl</span><br />
						<span>0318-307529</span><br />
					</div>	
				</div>
			</div>
		</div>
		
		
		<div class="footer-bottom">
			<div class="row">
			
					<p class="pull-left">Copyright © 2018 Autodemontage Veenendaal. Alle rechten voorbehouden.</p>
					<p class="pull-right" style="padding-right: 20%;">Ontworpen door <span><a target="_blank" href="http://www.steensolutions.nl">Steen Solutions</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

  
    <script src="/js/jquery.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/jquery.scrollUp.min.js"></script>
	<script src="/js/price-range.js"></script>
    <script src="/js/jquery.prettyPhoto.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
    <script src="/js/jquery.easyPaginate.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/kentekenplaat-jquery.min.js"></script>
    <script src="/js/kentekenplaat.min.js"></script>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b3e167a4af8e57442dc5d04/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

    <script>
       new Kentekenplaat(document.querySelector('#kentekenplaat'));
    </script>

    @yield('js')

    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5acd09fad7591465c7096019/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    @yield('js')
</body>
</html>