<accountbar id="accountbar">

                  @auth
                    <div class="left-sidebar categorie_layout">
                      <div class="categorie_headtext all_color"> 
                        <h3>Menu</h3>
                      </div>
                      <div class="panel-group category-products body_blok" id="accordian">
                        
                        <div class="panel-heading">
                          <br />
                          <h4 class="panel-title">
                              <a href="/account">Mijn account</a>
                          </h4>
                        </div>
                        <div class="panel-heading">
                          <h4 class="panel-title">
                              <a href="/bestellingen">Bestellingen</a>
                          </h4>
                        </div>
                        <div class="panel-heading">
                          <h4 class="panel-title">
                              <a href="/loguit">Uitloggen</a>
                          </h4>
                        </div>
                      </div>
                    </div>
                  @endauth
</accountbar>                  