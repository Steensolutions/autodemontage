<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Webshop - Autodemontage Veenendaal</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/prettyPhoto.css" rel="stylesheet">
    <link href="/css/price-range.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
	<link href="/css/main.css" rel="stylesheet">
	<link href="/css/responsive.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/kentekenplaat.min.css" />
	@yield('css')
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->




<body>
	<header id="header"><!--header-->
		<div class="header_top block_mobiel"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-3"></div>
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#">Maandag - Vrijdag: 09:00 - 17:00</a></li>
								<li><a href="#">Zaterdag: 09:00 - 15:00</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">


					<div class="col-sm-4 logo_allign">
						<a class="top_flag flags" href="/verandertaal/en"><img class="flag_img" src="/img/Flag_Eng.png"></a>
						<a class=top_flag href="/verandertaal/de"><img class="flag_img" src="/img/Flag_Ger.png"></a>
						<a class=top_flag href="/verandertaal/fr"><img class="flag_img" src="/img/Flag_Fra.png"></a>
						<a class=top_flag href="/verandertaal/nl"><img class="flag_img" src="/img/Flag_NL.png"></a>
						<div class="pull-left">
							<a href="/"><img class="logo autologo" src="/images/home/logo.png" alt="" /></a>
						</div>

					
					</div>
					<div class="col-sm-8">
						<div id="headermenu">
					<nav>
						<ul>
							<li>
								<a href="/account" class="link1"><span><i class="fa fa-user-circle-o" aria-hidden="true"></i> Mijn account</span></a>
							</li>
							<li>
								<a href="/winkelwagen" class="link2"><span><i class="fa fa-shopping-cart" aria-hidden="true"></i> Winkelwagen</span></a>
							</li>
							<li class="pc_versie">
								<a href="tel:0318307529" class="link3"><span><i class="fa fa-phone" aria-hidden="true"></i> 0318 - 307529
							</span></a>
							</li>
						</ul>
					</nav>
				</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	



		<div class="header-bottom"><!--header-bottom-->
			<div class="">
				<div id="menu-down" class="row" style="margin:0px !important;">
					<div class="mobile_menu">
						<div class="navbar-header">

							<div class="block_pc">

										</div>


											<button type="button" class="navbar-toggle mobile_navbar" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

						</div>

						<div class="mainmenu pull-left navbar_pc">

							<ul class="nav navbar-nav collapse navbar-collapse mobile_navbar_blok navbar_pc navbar_borders">


<!-- 								<img src="img/logo_lang.png"> -->
							
								<li><a href="/contact" @if (\Request::is('contact'))  class="active" @endif class="">CONTACT</a></li>
								<li><a href="/aanvraag" @if (\Request::is('aanvraag'))  class="active" @endif class="">AANVRAAG</a></li>
								<li><a href="/voorraad" @if (\Request::is('voertuigenvorraad'))  class="active" @endif class="">VOERTUIGEN</a></li>
								<li><a href="/onderdelen" @if (\Request::is('onderdelen'))  class="active" @endif class="">ONDERDELEN</a></li>
								<li class="active"><a href="/"  @if (\Request::is('/'))  class="active" @endif >HOME</a></li>


							</ul>
						</div>



					</div>

					</div>
				</div>
			</div>










<div class="mobile_versie">
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="mobile_navbar">
		
		<div class=logo_background>
			<a href="/"><img class="logo autologo" src="/images/home/logo.png" alt="" /></a>
		</div>
	    <li class="active">
	    	<a href="/"  @if (\Request::is('/'))  class="active" @endif >HOME</a>
	    </li>
		<li>
			<a href="/onderdelen" @if (\Request::is('onderdelen'))  class="active" @endif class="">ONDERDELEN</a>
		</li>
		<li class="navbar_tab_big">
			<a href="/voorraad" @if (\Request::is('voertuigenvoorraad'))  class="active" @endif class="">VOERTUIGEN VOORRAAD</a>
		</li>
		<li>
			<a href="/aanvraag" @if (\Request::is('aanvraag'))  class="active" @endif class="">AANVRAAG</a>
		</li>
		<li>
			<a href="/contact" @if (\Request::is('contact'))  class="active" @endif class="">CONTACT</a>
		</li>


                    <button class="nav_close" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="nav_close"></span>
                  </button>
    </div>
  </div>
</div>


		</div><!--/header-bottom-->

	</header><!--/header-->
	


