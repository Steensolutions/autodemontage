@extends('template.app')

@section('content')
<section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li><a href="/winkelwagen">Winkelwagen</a></li>
                  <li class="active">Afrekenen</li>
                </ol>
            </div>
            <div class="table-responsive">
                 <div class="col-ms-12">
        <table class="table table-striped winkelwagen">
         <thead>
              <tr>
                <td>Afbeelding</td>
                <td>Onderdeel</td>
                <td>Aantal</td>
                <td>Prijs</td>
              </tr>
        </thead>

            <tbody>
        @forelse($content as $content)
                <tr>
                    <td>
                        <img src="{{ $content->options->image }}small/1.jpg">
                    </td>
                    <td >
                        <a href="/product/{{ $content->id }}">{{ $content->name }}</a>
                        <p>{{ $content->options->car }}</p>
                    </td>
                    <td>
                        <p> {{$content->qty }}<p>
                    </td>
                    <td>
                        <p>€ {{ $content->price }}</p>
                    </td>
                    <td>
                        <a class="cart_quantity_delete" href="/remove-from-cart/{{ $content->rowId }}"><i class="fa fa-times"></i></a>


                    </td>
                </tr>
        @empty
        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </section> <!--/#cart_items-->

          <form action="/checkout/proceed" method="post">
        
                                @method('post')

                                @csrf

    <section id="cart_items">
        <div class="container">

            @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@auth
U bent ingelogd op uw account, en de accountgegevens zijn overgenomen naar het bestelformulier.<br />
@endauth
    <div class="shopper-informations">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <h3>Gegevens</h3>

                                <label for="type"> Accounttype </label>
                                <select name="type" @if(Auth::user()) value="{{Auth::user()->type}}" @endif class="form-control" required id="accounttype" onclick="acounttype()">
                                        <option value="not">Selecteer accounttype *</option>
                                        <option value="zakelijk">Zakelijk</option>
                                        <option value="particulier">Particulier</option>
                                </select>
                           

                                    <div id="zakelijk">
                                        <label for="bedrijfsnaam">Bedrijfsnaam: </label>
                                        <input name="bedrijfsnaam" class="form-control" @if(Auth::user()) value="{{Auth::user()->bedrijfsnaam}}" @endif type="text" placeholder="Bedrijfsnaam *">
                                    </div>


                                    <label for="aanhef">Aanhef: </label>
                                    <input name="aanhef" class="form-control" @if(Auth::user()) value="{{Auth::user()->aanhef}}" @endif type="text" placeholder="Aanhef">

                                    <label for="voornaam">Voornaam</label>
                                    <input name="voornaam" class="form-control" @if(Auth::user()) value="{{Auth::user()->voornaam}}" @endif type="text" placeholder="Voornaam *" required>

                                    <label for="tussenvoegsel">Tussenvoegsel </label>
                                    <input name="tussenvoegsel" class="form-control" @if(Auth::user()) value="{{Auth::user()->tussenvoegsel}}" @endif type="text" placeholder="Tussenvoegsel">

                                    <label for="achternaam">Achternaam </label>
                                    <input name="achternaam" class="form-control" @if(Auth::user()) value="{{Auth::user()->achternaam}}" @endif type="text" placeholder="Achternaam *" required>

                                    <label for="email">Email </label>
                                    <input name="email" class="form-control" @if(Auth::user()) value="{{Auth::user()->email}}" @endif type="email" placeholder="Email-adres *" required>

                                    <label for="telefoon">Telefoonnummer </label>
                                    <input name="telefoon" class="form-control" @if(Auth::user()) value="{{Auth::user()->telefoon}}" @endif type="tel" placeholder="Telefoonnummer *" required>
                        </div>
                    </div>
                    <div class="col-sm-3 clearfix">
                        <div class="bill-to">
                            <h3>Adresgegevens</h3>
                            <div class="form-one">


                                    <div id="zakelijk2">
                                        <label for="btwnummer">BTW-nummer </label>
                                        <input name="btwnummer" class="form-control" @if(Auth::user()) value="{{Auth::user()->btwnummer}}" @endif type="text" placeholder="btwnummer *">  
                                    </div>

                                    <label for="postcode">Postcode </label>
                                    <input name="postcode" class="form-control" type="text" @if(Auth::user()) value="{{Auth::user()->postcode}}" @endif  placeholder="Postcode *" required>

                                    <label for="huisnummer">Huisnummer </label>
                                    <input name="huisnummer" class="form-control" type="text" @if(Auth::user()) value="{{Auth::user()->huisnummer}}" @endif placeholder="Huisnummer *" required>

                                    <label for="toevoeging">Toevoeging</label>
                                    <input name="toevoeging" class="form-control" @if(Auth::user()) value="{{Auth::user()->toevoeging}}" @endif type="text" placeholder="Toevoeging">

                                    <label for="straatnaam">Straatnaam </label>
                                    <input name="straatnaam" class="form-control" @if(Auth::user()) value="{{Auth::user()->straatnaam}}" @endif type="text" placeholder="Straatnaam *" required>

                                    <label for="plaatsnaam">Plaatsnaam </label>
                                    <input name="plaatsnaam" class="form-control" @if(Auth::user()) value="{{Auth::user()->plaatsnaam}}" @endif type="text" placeholder="Plaats *" required>

                                    <label for="land">Land </label>
                                    <select name="land" @if(Auth::user()) value="{{Auth::user()->land}}" @endif class="form-control" required>
                                        <option>Nederland</option>
                                        <option>Engeland</option>
                                        <option>België</option>
                                        <option>Duitsland</option>
                                    </select>

                            </div>
                            </div>

                        
                    </div>                
                                        <div class="col-sm-3">
                        <div class="order-message">
                            <h3>Kenteken</h3>
                            <p>Voor de zekerheid noteren ontvangen wij graag uw kentekennummer om te kunnen waarschuwen wanneer u "niet passende" onderdelen koopt.</p>

                            <input name="kenteken" class="form-control" @if(Auth::user()) value="{{Auth::user()->kenteken}}" @endif type="text" placeholder="Kenteken" required>                            
                            <h3>Opmerkingen</h3>
                            <textarea class ="form-control" name="opmerkingen" placeholder="Heeft u vragen of opmerkingen over de bestelling, dan kunt u deze hier invullen." rows="8"></textarea>
                            
                        </div>  
                    </div>                  
                </div>
            </div>
        </div>
    </section>



    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>Afrekenen</h3>
                <p>Controleer hieronder de gegevens en ga door om af te rekenen.</p>
            </div>
            <div class="row">
                <div class="col-sm-6">
                  
<table class="table table-striped winkelwagen">
    <tbody>
                <tr>
                        <td>
                            <h5>Subtotaal</h5>
                        </td>
                        <td>
                            <p>€ {{ $price * (1/1.21) }}</p>
                        </td>
                   
                </tr>
                <tr>
                        <td>
                            <h5>Totaal inc btw. en verzendkosten</h5>
                        </td>
                        <td>
                            <p>€ {{ $price }}</p>
                        </td>
                    
                </tr>

            </tr>
    </tbody>                   
</table>



                            @if($price !== "0.00")
                            <div class="col-md-6 checkout_placing">
                            <button class="blokhref checkout_buttons" action="submit">Ga door naar betalen</button>   
                            </div>
                            @endif
                    
                </div>
            </div>
        </div>





    </section><!--/#do_action-->    

    </form>   
  

@endsection


@section('js')
                                <script>

    $("#zakelijk").hide();
    $("#zakelijk2").hide();


function acounttype() {

  $("#accounttype").change(function() {
    var val = $(this).val();
    if(val === "zakelijk") {
        $("#zakelijk").show();
        $("#zakelijk2").show();
    }

    else if(val === "particulier") {
        $("#zakelijk").hide();
        $("#zakelijk2").hide();

    }
  });
}
                                </script>
@endsection