@extends('template.app')

@section('content')
<section>
             <div class="container">
            <div class="row">

                @include('template.categoriebar')                      
                <div class="col-sm-9 padding-right">
                    @if(session('message'))
                    <br />
                      <div class='alert alert-danger'>
                          {{ session('message') }}
                      </div>
                    @endif
                    <div class="row catagorie_blok">
                        <div class="col col-md-12">

                            <div class="row features_items"><!--features_items-->

                                
                                <div class="categorie_headtext all_color"> 

                                    <h3 class="text-center">{{$name}}</h3>
                                </div>
                               
                @foreach ($products as $product)
                    <page>
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">

                                            @if(empty($product['media']['images']['root']))
                                                <img src="/img/noimage.png" class="growImage">
                                            @else
                                                <img src="{{ $product['media']['images']['root'] }}large/0.jpg" alt="">
                                            @endif

                                        @if(empty($product['properties']['partpropertyvalue'][0]['values']['string']))
                                            Onbekend
                                        @else
                                            <h2>€ {{ $product['properties']['partpropertyvalue'][0]['values']['string'] }}</h2>
                                        @endif

                                        @if(empty($product['partname']))
                                            <p> Onderdeel heeft geen naam</p>
                                        @else
                                            <p>{{ $product['partname'] }}</p>
                                            <p>{{ $product['makename'] }} {{ $product['modelname'] }}</p>
                                            <a href="/add-to-cart/{{ $product['partid'] }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i><div class="product_text">Toevoegen aan winkelmand</div></a>
                                            <a href="/product/{{ $product['partid'] }}" class="btn btn-default add-to-cart">Meer informatie</a>
                                        @endif

                                    </div>

                                    
                                </div>
                             
                            </div>
                        </div>
                    </page>
                @endforeach
                                
                            </div><!--features_items-->
                        </div>
                    </div> <!-- row-->

                    <br />

                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
<script>
    $('#features_items').easyPaginate({
    paginateElement: 'page',
    elementsPerPage: 9,
    effect: 'climb'
    });
</script>


@endsection