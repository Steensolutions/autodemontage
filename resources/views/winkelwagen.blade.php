@extends('template.app')

@section('content')

<div class=container>
    <div class="col-ms-8 margin_top_winkelwagen">
        <table class="table table-striped winkelwagen">

@if($price !== "0.00")
         <thead>
                 <tr>
                    <h1 >Winkelwagen</h1>
                </tr>

            <tbody>
              <tr class="table_titels">
                <td>Afbeelding</td>
                <td>Onderdeel</td>
                <td>Aantal</td>
                <td>Prijs</td>
                <td></td>
              </tr>

        @forelse($content as $content)
                <tr>
                    <td>
                        <img src="{{ $content->options->image }}small/1.jpg">
                    
                    </td>
                    <td >
                        <a href="/product/{{ $content->id }}">{{ $content->name }}</a>
                        <p>{{ $content->options->car }}</p>
                    </td>
                    <td>
                        <p> {{$content->qty }}</p>
                    </td>
                    <td>
                        <p>€ {{ $content->price }}</p>
                    </td>
                    <td>
                        <a class="delete_img" href="/remove-from-cart/{{ $content->rowId }}"><img src="/images/icons/PNG/delete.png"></a>


                    </td>
                </tr>
        @empty
        @endforelse


                <tr class="prijzen">
                    <td></td>
                    <td></td>
                        <td>
                            <h5>Subtotaal</h5>
                        </td>
                        <td>
                            € {{ $price * (1/1.2100000) }}
                        </td>
                    <td>
                    </td>
                </tr>
                <tr class="prijzen">
                    <td>                
                    </td>
                    <td>                
                    </td>
                        <td>
                            <h5>Totaal inc btw. </h5>
                        </td>
                        <td style="font-weight: 500; padding:0px;">€ {{ $price }}
                        </td>
                    <td>
                    </td>
                </tr>

            </tr>

         </tbody>

@else

         <thead>
                <tr>    
                    <h1 >Winkelwagen</h1>
                    <p> Uw winkelwagen is helaas leeg</p>
                </tr>
        </thead>

@endif
        </table>
    </div>
</div>





<div class=container>
    <div class="col-ms-12 margin_top_winkelwagen">

        <div class="row">
            </div>
                
                @if($price !== "0.00")

            <div class="col-md-6 checkout_placing">
                    <p><a class="blokhref blockhrefone checkout_buttons" href="javascript:javascript:history.go(-1)">Ga verder met winkelen</a>
            </div>

            <div class="col-md-6 checkout_placing">
                    <p><a class="blokhref blockhrefone checkout_buttons" href="/winkelwagen/leeg">Leeg winkelwagen</a>
            </div>

            <div class="col-md-6 checkout_placing">
                    <a class="blokhref checkout_buttons" href="/checkout">Ga door naar afrekenen</a></p>
            </div>
            
            <div class="col-md-6 checkout_placing">
                    <a class="blokhref checkout_buttons" href="/account">Mijn account</a></p>
            </div>

                 @endif
            

            <div class="col-md-12 onder_button">
                <p><a class="blokhref" href="/aanvraag">Product niet gevonden? Plaats een aanvraag!</a></p>
            </div>
        </div>

    </div>


</div>
    </section><!--/#do_action-->    
@endsection
