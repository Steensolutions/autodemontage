@extends('template.app')

@section('content')
<section>
        <div class="container">
            <div class="row">
                
                <div class="col-sm-9">
<div class="col-md-12">
                    <div class="top_blok">
                       <div class="top_blok_left col-sm-12">
                     <div id="content">

                  <!-- Content -->
                    <article>
                      <h2>Algemene voorwaarden</h2>
                      <p>

</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 1 TOEPASSELIJKHEID</p>
1.1 De onderstaande voorwaarden maken deel uit van alle rechtsverhoudingen waarbij wij partij zijn, in het bijzonder maar niet uitsluitend overeenkomsten tot het verkopen en/of leveren van gebruikte, gereviseerde en nieuwe voertuigonderdelen, alsmede overeenkomsten tot het verkopen en/of leveren van gebruikte voertuigen, met of zonder schade en/of de uitvoering van zodanige overeenkomsten.<br>
1.2 Afwijkingen en/of aanvullingen op deze algemene voorwaarden binden ons slechts voor zover wij de gelding daarvan schriftelijk en uitdrukkelijk hebben vastgelegd. Bij verwijzing door de koper naar eigen voorwaarden, gelden bij uitsluiting de onderhavige voorwaarden, tenzij uitdrukkelijk anders overeengekomen.<br>
1.3 Onze vertegenwoordigers mogen van deze voorwaarden niet afwijken dan krachtens uitdrukkelijke schriftelijke volmacht, voor iedere overeenkomst afzonderlijk te verlenen.<br>
1.4 Indien de koper een rechtspersoon, vennootschap onder firma of commanditaire vennootschap is, wordt degene die voor haar optreedt geacht zich persoonlijk als hoofdelijk aansprakelijk debiteur te hebben verbonden, tenzij wij schriftelijk en uitdrukkelijk anders overeenkomen.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 2 PRIJZEN</p>
2.1 Tenzij anders aangegeven zijn alle prijzen van gebruikte onderdelen en gebruikte auto’s, exclusief aftrek of korting en inclusief B.T.W. al dan niet berekend via de B.T.W.-margeregeling van het demontagebedrijf. Tenzij anders aangegeven zijn alle prijzen van gereviseerde en nieuwe onderdelen exclusief aftrek of korting en exclusief B.T.W.<br>
2.2 Prijzen zijn berekend voor levering af bedrijf, tenzij uitdrukkelijk anders vermeld.<br>
2.3 Opgave van prijzen, van te koop aangeboden zaken en van specificaties vervat in algemene aanbiedingen, zoals catalogi, prijslijsten en ander drukwerk, zijn vrijblijvend. Zij binden ons niet en de koper kan zich daarop niet beroepen, tenzij anders overeengekomen of aangegeven.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 3 LEVERING</p>
3.1 Levering geschiedt af werkplaats, magazijn of winkel ter onzer keuze. Op de koper rust een afnameplicht, tenzij wij hierbij geen redelijk belang hebben.<br>
3.2 Het risico van het verkochte gaat over op het moment dat de zaken klaar staan voor levering dan wel verzending.<br>
3.3 Het verkochte zal voetstoots worden geleverd in de staat waarin het zich bij het sluiten van de overeenkomst bevindt.<br>
3.4 Vervoer van onderdelen door ons vindt geheel voor rekening en risico van de koper plaats.<br>
3.5 De retourkosten voor verkeerd geleverde onderdelen, zijn ten alle tijden voor de klant.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 4 LEVERINGSTERMIJN</p>
4.1 Levertijden worden in overleg en bij benadering door ons vastgesteld. Levertijden zijn nimmer te beschouwen als een fatale termijn. De levertijd gaat in bij mondelinge en schriftelijke- orderbevestiging.<br>
4.2 Indien de koper niet binnen vier weken nadat wij koper hebben geïnformeerd dat het gekochte gereed staat voor afhalen, het gekochte afhaalt, zal de overeenkomst zonder rechterlijke tussenkomst zijn ontbonden, tenzij wij schriftelijk de koper laten weten nakoming te verlangen.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 5 BETALING</p>
5.1 Tenzij anders overeengekomen geschiedt betaling contant of via de bank.<br>
5.2 Bij aankoop op factuur dient betaling te zijn ontvangen binnen veertien dagen na factuurdatum.<br>
5.3 Indien op de vervaldag geen of niet-tijdige dan wel niet-volledige betaling heeft plaatsgevonden, raakt de koper, zonder dat ingebrekestelling of aanmaning nodig is, in verzuim en is hij over het achterstallig bedrag direct opeisbaar verschuldigd de wettelijke rente per maand of gedeelte van een maand, gerekend vanaf de vervaldag.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 6 EIGENDOMSVOORBEHOUD</p>
6.1 De koper is niet gerechtigd geleverde zaken -zolang deze niet betaald zijn- aan derden door te leveren, in bruikleen te geven, te verpanden of in eigendom over te dragen, behoudens voor zover dit plaatsvindt in de normale bedrijfsuitoefening door de koper.<br>
6.2 De koper draagt het risico voor niet betaalde goederen ten aanzien van alle schade, directe en indirecte, welke daaraan door hem zelf of enig ander zal worden toegebracht.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 7 TEKORTKOMING / KLACHTEN</p>
7.1 Koper is gehouden om leveringen na uitvoering nauwkeurig op eventuele tekortkomingen in de vorm van afwijkingen van specificaties en overige waarneembare tekortkomingen te controleren. Geconstateerde tekortkomingen dienen binnen 8 dagen na ontdekking aan ons gemeld te worden. Deze melding dient schriftelijk te geschieden en vergezeld te gaan van een omschrijving van de geconstateerde tekortkoming, onder vermelding van de factuur en het factuurnummer. Ten einde de garantie van toepassing te doen zijn is de uiterste meldingsdatum 3 dan wel 6 maanden na de koop overeenkomstig de in artikel 12.4 genoemde garantietermijnen.<br>
7.2 Koper dient ons in staat te stellen de geconstateerde tekortkoming te controleren. Niet voldoen aan het in dit artikellid bepaalde leidt tot verval van het recht van de koper zich te beroepen op de geconstateerde tekortkoming.<br>
7.3 Voor zover de wet dit toelaat, leveren tekortkomingen in het geleverde de koper geen grond tot ontbinding van de overeenkomst, tenzij wij er na herhaalde pogingen niet in slagen de tekortkomingen aanvaardbaar op te heffen. In dat geval is de koper bevoegd de overeenkomst te ontbinden indien en voor zover instandhouding in redelijkheid niet van hem kan worden gevergd.<br>
7.4 Koper dient de kosten voor ongegronde klachten aan ons te vergoeden.<br>
7.5 Een beroep op een tekortkoming geeft koper niet het recht zijn betalingsverplichting op te schorten.<br>
7.6 De bepalingen van dit artikel 7 gelden met inachtneming van het bepaalde in artikel 12.6 van deze voorwaarden.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 8 OVERMACHT</p>
8.1 Indien wij geheel of gedeeltelijk tekort schieten in de nakoming van de verplichting jegens koper, kan dit tekortschieten niet aan ons worden toegerekend indien ons de uitvoering van de overeenkomst wordt bemoeilijkt resp. onmogelijk wordt gemaakt door een -al dan niet voorzienbare- omstandigheid die buiten onze macht is gelegen, zoals, maar niet beperkt tot:<br>
– tekortschieten door toeleveranciers/transporteurs;<br>
– oorlog, oproer of daarop gelijkende situaties;<br>
– sabotage, boycot, staking of bezetting;<br>
– machineschade;<br>
– diefstal uit de magazijnen;<br>
– bedrijfsstoornissen;<br>
8.2 Als zich een situatie voordoet genoemd in lid 1 van dit artikel zijn wij niet aansprakelijk voor de eventuele daaruit voor de koper voortvloeiende schade en kunnen wij naar eigen keuze de nakoming van onze verplichtingen opschorten resp. de overeenkomst zonder rechtelijke tussenkomst geheel of gedeeltelijk ontbinden zonder tot enige schadevergoeding gehouden te zijn.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 9 GEBRUIK VAN DE ZAAK</p>
9.1 Koper dient de geleverde zaak te gebruiken overeenkomstig de aard en bestemming en met inachtneming van alle wettelijke gebruiksvoorschriften en, voor zover van toepassing, door ons voorgeschreven gebruiksvoorschriften.<br>
9.2 Indien koper de geleverde zaak niet overeenkomstig het in lid 1 van dit artikel bepaalde gebruikt en koper ons aansprakelijk stelt voor schade geleden in verband met het gebruik van de geleverde zaak, dient koper te bewijzen dat schade het gevolg is van een gebrek in de door ons geleverde zaak en niet van het gebruik anders dan overeenkomstig lid 1 van dit artikel.<br>
9.3 Onverminderd het bepaalde in artikel 10 en lid 2 van dit artikel zijn wij nimmer aansprakelijk voor letselschade indien de koper heeft gehandeld in strijd met het in lid 1 van dit artikel bepaalde. Koper dient ons te vrijwaren tegen aanspraken van werknemers of andere derden, in het bijzonder afnemers, wanneer deze geen kennis hebben genomen van de uit lid 1 van dit artikel voortvloeiende gebruiksvoorschriften.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 10 AANSPRAKELIJKHEID</p>
10.1 Voor schade uit of in verband met leveringen waarvoor wij rechtens aansprakelijk kunnen worden gehouden geldt, voor zover dwingendrechtelijke bepalingen niet anders meebrengen, dat onze aansprakelijkheid het factuurbedrag niet te boven gaat.<br>
10.2 Schade, voor zover bestaande uit gederfde winst of verminderde opbrengst en alle andere indirecte schade of gevolgschade, zoals bedrijfsschade, schade aan andere voertuigdelen of enige door koper aan derden verschuldigde schadevergoeding of boete, komt in geen geval voor vergoeding in aanmerking, behoudens andersluidende dwingendrechtelijke bepalingen.<br>
10.3 Op straffe van verval van het recht op schadevergoeding wordt ons alle gewenste medewerking verleend bij het onderzoek naar oorzaak, aard en omvang van de schade waarvoor vergoeding wordt gevorderd.<br>
10.4 Artikel 12.7 is van overeenkomstige toepassing.
<p>
A</p><p style="font-size: 20px; font-weight: bold;">RTIKEL 11 ONTBINDING</p>
11.1 Gehele of gedeeltelijke ontbinding van de overeenkomst vindt uitsluitend plaats door een schriftelijke verklaring van één van de daartoe gerechtigden. Alvorens de koper een schriftelijke ontbindingsverklaring tot ons richt, zal de koper te allen tijde ons eerst schriftelijk in gebreke moeten stellen en ons een redelijke termijn gunnen om alsnog onze verplichtingen deugdelijk na te komen.<br>
11.2 De koper heeft geen recht de overeenkomst geheel of gedeeltelijk te ontbinden of zijn verplichtingen op te schorten, indien hij zelf reeds in verzuim was met de nakoming van zijn verplichtingen. Voor consumentenkopers laat deze bepaling hun eventuele bevoegdheid tot opschorting op grond van enig wettelijke bepaling onverlet.<br>
11.3 Indien wij instemmen met ontbinding, zonder dat er sprake is van verzuim van onze zijde, hebben wij het recht op vergoeding van alle vermogensschade, zoals kosten, gederfde winst en redelijke kosten ter vaststelling van schade en aansprakelijkheid.<br>
11.4 In geval van gedeeltelijke ontbinding kan de koper geen aanspraak maken op ongedaanmaking van reeds door ons verrichte prestaties en hebben wij onverkort recht op betaling voor de reeds door ons verrichte prestaties onverminderd het recht van ons om onze prestaties ongedaan te maken en schadevergoeding te vorderen.<br>
11.5 De koper heeft geen recht om de overeenkomst te ontbinden na aankoop van elektrische onderdelen.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 12 GARANTIES</p>
12.1 Met uitzondering van elektronische onderdelen komen voor garantie in aanmerking, verkochte en geleverde gebruikte, gereviseerde en/of nieuwe voertuigdelen. Niet voor garantie komen in aanmerking verkochte en geleverde (gebruikte) schadeauto’s en elektronische onderdelen.<br>
12.2 De koper kan slechts rechten ontlenen aan een garantie indien hij bewijst dat hij de zaak van ons heeft gekocht. Dit bewijs kan geleverd worden door overlegging aan ons van de desbetreffende koopovereenkomst c.q. factuur en indien toepasselijk de ter zake opgemaakte garantiekaart. Indien het een zaak betreft die door ons van een merk of een kenteken is voorzien, kan de koper slechts rechten ontlenen aan een garantie indien bij een beroep op deze garantie het bedoelde merk of kenteken ongeschonden is.<br>
12.3 Aanspraken van de koper uit hoofde van een garantie zijn niet overdraagbaar aan derden.<br>
12.4 Wij garanderen de deugdelijkheid en bruikbaarheid van de door ons geleverde gebruikte voertuigdelen, gedurende drie maanden na de aankoop en de door ons geleverde nieuwe en gereviseerde onderdelen, gedurende zes maanden na aankoop, alles onder de voorwaarden genoemd in dit artikel. De koper heeft het recht om bij gebleken ondeugdelijkheid gedurende voornoemde garantietermijn de geleverde zaak aan ons aan te bieden ter vervanging of herstel, ter keuze van ons, één en ander met inachtneming van de overige artikelen van deze algemene voorwaarden en in het bijzonder lid 6 van dit artikel. Indien vervanging of herstel in redelijkheid niet mogelijk is, hebben wij het recht om het factuurbedrag te retourneren.Wij hebben het recht om in plaats van reparatie of vervanging te kiezen voor het retour nemen van de auto tegen terugbetaling van het desbetreffende factuurbedrag. In geval van ondeugdelijkheid van gereviseerde en nieuwe onderdelen zullen wij eveneens de in- en uitbouwkosten van het betreffende onderdeel vergoeden tot een maximum van tweemaal het factuurbedrag.<br>
12.5 De door koper na herstel/vervanging ontvangen zaak komt opnieuw voor garantie in aanmerking.<br>
12.6 De koper kan geen aanspraak maken op garantie:<br>
a. indien de koper onjuiste of onvoldoende informatie heeft verstrekt met betrekking tot het merk en type aanduiding van het gekochte en/of het voertuig waarvoor het onderdeel bestemd is;<br>
b. indien de inbouw van het gekochte ondeugdelijk is geschied;<br>
c. indien de koper werkzaamheden, zoals maar niet beperkt tot reparatie, wijziging en demontage aan het gekochte heeft verricht resp. heeft doen verrichten;<br>
d. indien er sprake is van een inbouw c.q. gebruik voor een ander doel dan waartoe het gekochte dient;<br>
e. indien er sprake is van inbouw in voertuigen welke afwijken van de standaardspecificaties van de fabrikant;
f. indien er sprake is van ondeugdelijk en/of ondeskundig gebruik van het gekochte c.q. bij gebruik van het voertuig waarin het gekochte is ingebouwd voor andere doeleinden dan waartoe het voertuig in het normale verkeer gebezigd wordt (snelheidstesten, betrouwbaarheidsproeven, te zware belasting in verband met combinatie personenwagen en aanhangwagen c.q. caravan e.d.)<br>
g. indien gehandeld is in strijd met enig andere bepaling uit deze algemene voorwaarden voor zover deze op straffe van verval van rechten zijn voorgeschreven.<br>
12.7 De koper kan aan een garantie geen recht ontlenen op schadevergoeding van welke aard dan ook, behoudens voor zover wij op grond van de wet of de toepasselijke algemene voorwaarden daartoe gehouden zijn.<br>
12.8 In individuele gevallen zijn wij bereid om in overleg af te wijken van de garantievoorwaarden. Wij zijn slechts aan afwijkende garantiebepalingen gebonden indien wij dit schriftelijk zijn overeengekomen.<br>
<p>
</p><p style="font-size: 20px; font-weight: bold;">ARTIKEL 13 GESCHILLEN</p>
13.1 Op alle transacties tussen ons en koper is bij uitsluiting het Nederlands recht van toepassing.<br>
13.2 De klachtenprocedure laat het beroep van de koper op de bevoegde rechter onverlet.<br>
<p>
</p><p style="font-size: 15px; font-weight: bold;">Autodemontage Veenendaal B.V.<br>
Turbinestraat 22A<br>
3903LW Veenendaal</p>

                    </article> <br />

                </div>
                           </div>
                       </div>

                   </div>
                   </div>
                </div>
    </section>
@endsection
