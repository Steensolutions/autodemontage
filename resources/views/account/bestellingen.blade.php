@extends('template.app')

@section('content')
<section>
  <div class="container">
    <br />
    <div class="row">
      <div class="col-sm-3">
        @include('template.accountbar')
      </div>
      <div class="col-sm-9 padding-right">
        <div class="row catagorie_blok">
          <div class="col col-md-12">
            <div class="row features_items"><!--features_items-->
              <div class="categorie_headtext"> 
                <h3 class="text-center">Mijn bestellingen</h3>
              </div>
              <div class="col-sm-1"></div>
              <div class="col-sm-10">
                @auth

                  <p><b>Uw bestellingen:</b></p>

                  <table>
                    <tr>
                      <th>Bestelnummer</th> 
                      <th>Datum</th>
                      <th>Bedrag</th>
                    </tr>

                  @foreach($bestellingen as $bestelling)
                  <tr>
                    <td><a href="/bestelling/{{$bestelling->order_id}}">{{$bestelling->order_id}}</a></td>
                    <td>{{$bestelling->created_at}}</td>
                    <td>{{$bestelling->prijs}}</td>
                  </tr>

                  @endforeach

                </table>

                <br />

                <p>Heeft u vragen over uw bestellingen, neem dan contact op met autodemontage Veenendaal. U kunt contact opnemen via de telefoon of via het <a href="/contact">contact</a> formulier.</p> <br />

                  @endauth

                  @guest

                  <p>U bent momenteel niet ingelogd. Maak een nieuw account aan of meld je aan via het onderstaande formulier.</p>

                  @endguest


                  <br />

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>

@endsection

@section('css')
<style>
table { 
  width: 750px; 
  border-collapse: collapse; 
  margin:20px auto;
  }

/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #eee; 
  }

th { 
  background: #3498db; 
  color: white; 
  font-weight: bold; 
  }

td, th { 
  padding: 10px; 
  border: 1px solid #ccc; 
  text-align: left; 
  font-size: 18px;
  }

/* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

  table { 
      width: 100%; 
  }

  /* Force table to not be like tables anymore */
  table, thead, tbody, th, td, tr { 
    display: block; 
  }
  
  /* Hide table headers (but not display: none;, for accessibility) */
  thead tr { 
    position: absolute;
    top: -9999px;
    left: -9999px;
  }
  
  tr { border: 1px solid #ccc; }
  
  td { 
    /* Behave  like a "row" */
    border: none;
    border-bottom: 1px solid #eee; 
    position: relative;
    padding-left: 50%; 
  }

  td:before { 
    /* Now like a table header */
    position: absolute;
    /* Top/left values mimic padding */
    top: 6px;
    left: 6px;
    width: 45%; 
    padding-right: 10px; 
    white-space: nowrap;
    /* Label the data */
    content: attr(data-column);

    color: #000;
    font-weight: bold;
  }

}
</style>
@endsection
