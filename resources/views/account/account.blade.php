@extends('template.app')

@section('content')
<section>
        <div class="container">
          <br />
            <div class="row">


                <div class="col-sm-3">
                  @include('template.accountbar')
                </div>

              <div class="col-sm-6 padding-right">
                    <div class="row catagorie_blok">
                        <div class="col col-md-12">
                            <div class="row features_items"><!--features_items-->
                                <div class="categorie_headtext"> 
                                    <h3 class="text-center">Mijn account</h3>
                                </div>
                              <div class="col-sm-1"></div>
                              <div class="col-sm-10">
                                @auth
                                  <p>Welkom op uw persoonlijke omgeving.</p><br />
                                  <p>In uw persoonlijke omgeving kunt u oude bestellingen bekijken en gemakkelijk nieuwe bestellingen doen met je opgeslagen gegevens.</p>
                                  <br />
                                  <p>Heeft u vragen over uw bestellingen, neem dan contact op met autodemontage Veenendaal. U kunt contact opnemen via de telefoon of via het <a href="/contact">contact</a> formulier.</p> <br />
                                @endauth
                                @guest

                                  <p>U bent momenteel niet ingelogd. <a href="/registreren">Maak een nieuw account</a> aan of meld je aan via het onderstaande formulier.</p>
                              </div>
                              <div class="col-sm-1"></div>
                              <div class="col-sm-1"></div>
                              <div class="col-sm-10">
                                  <br />
                                  <form method="POST" action="{{ route('login') }}">
                                        @csrf

                                        <div class="form-group row">
                                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('Email adres') }}</label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Wachtwoord') }}</label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6 offset-md-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Login') }}
                                                </button>

                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('Wachtwoord vergeten?') }}
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                  </div>
                              </div>
                                @endguest

                            </div>
                        </div>
                    </div>
              </div>

            </div>
        </div>
        <br />


</section>
@endsection

@section('css')
<style>
table { 
  width: 750px; 
  border-collapse: collapse; 
  margin:50px auto;
  }

/* Zebra striping */
tr:nth-of-type(odd) { 
  background: #eee; 
  }

th { 
  background: #3498db; 
  color: white; 
  font-weight: bold; 
  }

td, th { 
  padding: 10px; 
  border: 1px solid #ccc; 
  text-align: left; 
  font-size: 18px;
  }

/* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

  table { 
      width: 100%; 
  }

  /* Force table to not be like tables anymore */
  table, thead, tbody, th, td, tr { 
    display: block; 
  }
  
  /* Hide table headers (but not display: none;, for accessibility) */
  thead tr { 
    position: absolute;
    top: -9999px;
    left: -9999px;
  }
  
  tr { border: 1px solid #ccc; }
  
  td { 
    /* Behave  like a "row" */
    border: none;
    border-bottom: 1px solid #eee; 
    position: relative;
    padding-left: 50%; 
  }

  td:before { 
    /* Now like a table header */
    position: absolute;
    /* Top/left values mimic padding */
    top: 6px;
    left: 6px;
    width: 45%; 
    padding-right: 10px; 
    white-space: nowrap;
    /* Label the data */
    content: attr(data-column);

    color: #000;
    font-weight: bold;
  }

}
</style>
@endsection
