@extends('template.app')

@section('content')
<section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li class="active">Order: {{ $bestelling->order_id }} </li>
                </ol>
            </div>
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">Product</td>
                            <td class="description"></td>
                            <td class="price">Prijs</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($products as $content)
                        <tr>
                            <td class="cart_product">
                                <img src="{{ $content->image }}small/1.jpg">
                             
                            </td>
                            <td class="cart_description">
                                <h4>{{ $content->naam }}</h4>
                                <p>{{ $content->auto }}</p>
                            </td>
                            <td class="cart_price">
                                <p>€ {{ $content->prijs }}</p>
                            </td>
  
                        </tr>
                        @empty
                        <tr>
                        <td class="cart_description">
                         <h4 class="no-found">Er zijn geen producten gevonden, er gaat wat mis met de bestelling.</h4>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </section> <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>Afgerekend</h3>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="total_area">
                        <ul>
                            <li>Totaal ex btw. <span>€ {{ $price * (1/1.21) }}</span></li>
                            <li>Totaal inc btw. <span>€ {{ $price }}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#do_action-->    
@endsection
