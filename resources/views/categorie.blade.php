@extends('template.app')

@section('content')
<section>

        <div class="container">
            <div class="row">
                
                @include('template.categoriebar')

                <div class="col-sm-9 padding-right">

                    <div class="row">
                        <div class="col col-md-12 all_color searchblok">
                            <form id="searchform">
                                
                                <div class="col col-md-10">
                                    <input type="text" class="search-query form-control" placeholder="Zoek" id="search-criteria" >
                                </div>

                                <div class="col col-md-2">
                                    <button type="submit" class="search_button">Zoek</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    

                    <div class="row catagorie_blok">
                        <div class="col col-md-12">

                            <div class="row features_items"><!--features_items-->

                                
                                <div class="categorie_headtext"> 

                                    <h3 class="text-center">{{ $name }} {{ $makename }} {{$modelname}}</h3>
                                </div>
                               
                                    @foreach($cats as $cat)
                                        @if(empty($cat['partname']))
                                        @else
                                            <div class="col-md-4 subcategorie_text">
                                            <a class="title" href="/categorie/sub/{{ $cat['partname'] }}@if($filtermodel)?model={{$modelname}}@endif @if($filtermake)&make={{$makename}} @endif ">
                                            <b>{{ $cat['partname'] }} </b> ({{ $cat['rows'] }})</a>
                                            </div>
                                        @endif
                                    @endforeach
                                
                            </div><!--features_items-->
                        </div>
                    </div> <!-- row-->

                    <br />

                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')





<script>

$('#search').click(function(){
    $('.subcategorie_text').hide();
    var txt = $('#search-criteria').val();
    $('.subcategorie_text').each(function(){
       if($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1){
           $(this).show();
       }
    });
});



$('#searchform').submit(function () {
 return false;
});
</script>

@endsection