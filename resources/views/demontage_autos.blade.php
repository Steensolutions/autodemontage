@extends('template.app')

@section('content')


<div class="container">
    <div class="col-sm-12 storage_blok">
        <div class="row">
            <div class="col-sm-12">
      

         <h1> <i class="fa fa-ambulance"></i>Beschikbare auto's voor demontage</h1>
         <p> Maak een keuze uit een van de onderstaande auto's</p>


<form action="/carfilter" method="get">
    <div class="form-group">Merk
        <select name="make" class="form-control" id="selectmake">
          <option value="/demontage_autos">Selecteer een merk.</option>
          @foreach($categoriesfilter as $categorie)

            @if(($categorie['count'] < 2))
            
            @else
            <option value="/demontage_autos/1/{{$categorie['name']}}">{{$categorie['name']}}</option>
            @endif
            
          @endforeach

        </select>
    </div>

</form>     
            </div>
        </div>




<script type="text/javascript">
    window.onchange = function(){
        location.href=document.getElementById("selectmake").value;
    }       
</script>

    <div class="row">
    @foreach($makes as $make)

        @if($make['state'] === "damaged")
        @else

            <div class="part_blok voorraad text-center">

                @if(empty($make['media']['images']['root']))
                    <img src="/images/home/logo.png" class="voorraad_logo">
                @else
                    <img src="{{ $make['media']['images']['root'] }}/large/0.jpg" class="voorraad_img" alt="">
                @endif

                <div class="voorraad_text">
                    <a href="{{ route('showauto', $make['vehicleid'])}}">{{$make['makename']}} {{$make['modelname']}} </a>
                    <p>{{$make['properties']['propertyvalue'][0]['values']['string']}}</p>

                </div>
            </div>
        @endif

    @endforeach
    </div>



            <div class="col-sm-12 text-center"> 
            @if($amount > 1)
                <a href="/auto/{{ $amount - 1 }}">vorige</a>
            @else
            @endif

                {{ $amount}}
                
            @if($amount * 20 < $amount_cars)
                <a href="/auto/{{ $amount + 1 }}">volgende</a>
            @else
            @endif
                

            </div>

        </div>
    </div>
</div>


@endsection

@section('js')
@endsection