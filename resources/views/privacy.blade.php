@extends('template.app')

@section('content')
<section>
        <div class="container">
            <div class="row">
                
                <div class="col-sm-9">
<div class="col-md-12">
                    <div class="top_blok">
                       <div class="top_blok_left col-sm-12">
                        <div id="content">

                  <!-- Content -->
                    <article>
                      <h2>Privacy policy</h2>
                      <p>

Autodemontage Veenendaal B.V. respecteert de privacy van alle gebruikers van haar site en draagt er zorg voor dat de persoonlijke informatie die u ons verschaft vertrouwelijk wordt behandeld. Wij gebruiken uw gegevens om de bestellingen zo snel en gemakkelijk mogelijk te laten verlopen. Voor het overige zullen wij deze gegevens uitsluitend gebruiken met uw toestemming. Autodemontage Veenendaal B.V. zal uw persoonlijke gegevens niet aan derden verkopen en zal deze uitsluitend aan derden ter beschikking stellen die zijn betrokken bij het uitvoeren van uw bestelling.<br>
</p><p>
</p><p style="font-size: 15px; font-weight: bold;"> Autodemontage Veenendaal B.V. gebruikt de verzamelde gegevens om haar klanten de volgende diensten te leveren:</p>
<p>
  Als u een bestelling plaatst, hebben we uw naam, e-mailadres, afleveradres en betaalgegevens nodig om uw bestelling uit te voeren en u van het verloop daarvan op de hoogte te houden. Om het winkelen bij Autodemontage Veenendaal B.V. zo aangenaam mogelijk te laten zijn, slaan wij met uw toestemming uw persoonlijke gegevens en de gegevens met betrekking tot uw bestelling en het gebruik van onze diensten op. Hierdoor kunnen wij de website personaliseren.<br>
</p><p>
  Wij gebruiken uw e-mailadres om u te informeren over de ontwikkeling van de website en over speciale aanbiedingen en acties. Als u hier niet langer prijs op stelt, kunt u zich uitschrijven op onze website. Als u bij Autodemontage Veenendaal B.V. een bestelling plaatst bewaren wij uw gegevens op onze server. U kunt een gebruikersnaam en wachtwoord opgeven zodat uw naam en adres, telefoonnummer, e-mailadres, aflever- en betaalgegevens, zodat u deze niet bij iedere nieuwe bestelling hoeft in te vullen. Gegevens over het gebruik van onze site en de feedback die we krijgen van onze bezoekers helpen ons om onze site verder te ontwikkelen en te verbeteren.
</p><p>
  Als u reageert op een actie of prijsvraag, vragen wij uw naam, adres en e-mailadres. Deze gegevens gebruiken we om de actie uit te voeren, de prijswinnaar(s) bekend te maken, en de respons op onze marketingacties te meten.
</p><p>
</p><p style="font-size: 15px; font-weight: bold;">Autodemontage Veenendaal B.V. verkoopt uw gegevens niet:</p>
<p>
  Autodemontage Veenendaal B.V. zal uw persoonlijke gegevens niet aan derden verkopen en zal deze uitsluitend aan derden ter beschikking stellen die zijn betrokken bij het uitvoeren van uw bestelling. Onze werknemers en door ons ingeschakelde derden zijn verplicht om de vertrouwelijkheid van uw gegevens te respecteren.
</p><p>
</p><p style="font-size: 15px; font-weight: bold;">Cookies</p>
<p>
  Wanneer u onze website bezoekt kunnen wij automatisch enige informatie opslaan op uw computer in de vorm van een cookie om u automatisch bij een volgend bezoek te herkennen. Cookies helpen om de inhoud van onze websites beter af te stemmen op uw behoeften of uw wachtwoord of code zodanig op te slaan zodat u het niet elke keer opnieuw hoeft vast te leggen. Indien u niet wilt dat wij cookies gebruiken, dient u in uw browser \’cookies off\’ in te stellen. Indien u de cookies accepteert, zullen zij gedurende vijf jaar in uw computer blijven, tenzij u de cookies verwijdert. Het uitzetten van cookies kan uw gebruik van onze website en diensten beperken.
</p><p>
  Indien u nog vragen mocht hebben over de Privacy Policy van Autodemontage Veenendaal B.V., dan kunt u contact met ons opnemen. Onze klantenservice helpt u verder als u informatie nodig heeft over uw gegevens of als u deze wilt wijzigen. In geval wijziging van onze Privacy Policy nodig mocht zijn, dan vindt u op deze pagina altijd de meest recente informatie.

</p><p>
</p><p style="font-size: 15px; font-weight: bold;">Autodemontage Veenendaal B.V.<br>
Turbinestraat 22A<br>
3903LW Veenendaal</p>

                    </article>
<br />
                </div>

                           </div>
                       </div>

                   </div>
                   </div>
                </div>
    </section>
@endsection
