@extends('template.app')

@section('content')


<section>

  <div class="container">
    <div class="row">
      <div class="col-sm-12 storage_blok">
  
        <div>
           <h1> <i class="fa fa-wrench"></i> Beschikbare onderdelen voor een {{ $_GET['make'] }} {{ $_GET['model'] }}</h1>
           <p> Maak een keuze uit een van de onderstaande subcategorieen</p>
        </div>


  @foreach($categoriesfilter as $categorie)
<a href="/categorie/{{ $categorie['categoryid'] }}?make={{ $_GET['make'] }}&model={{ $_GET['model'] }}">
<div class="part_blok text-center"> 
                    <div class="storage_text">
                         <p>{{ $categorie['categoryname'] }}</p>
                       </div>
            <img class="storage_img" src="/images/icons/SVG/{{ App\Api::getImage($categorie['categoryname']) }}.svg">
                </div>
</a>


@endforeach
        </div>
      </div>
    </div>

</section>






@endsection


