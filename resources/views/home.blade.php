@extends('template.app')

@section('content')

<section>

<br />

  <div class="container">

                    @if(session('message'))
                      <div class='alert alert-danger'>
                          {{ session('message') }}
                      </div>
                    @endif


            
<div class="col-sm-12 mobile_top blok_full_layout all_color">

        <ul class="top_blok">
            <li class="active"> <a class="top_tab tableft text-center" href="#tab1" data-toggle="tab">KENTEKEN / MERK & MODEL</a></li>                              
            <li> <a class="top_tab text-center" href="#tab2" data-toggle="tab">ARTIKELNUMMER</a></li>
                                  
            <li> <a class="top_tab text-center" href="#tab3" data-toggle="tab">VERSNELLINGSBAKCODE</a></li>

            <li> <a class="top_tab tabright text-center" href="#tab4" data-toggle="tab">MOTORCODE</a></li>
        </ul>



        <div class="tab-content bot_blok all_color">
  <div class="tab-pane fade in active tab_layout" id="tab1">

              <div class="col col-md-6 kenteken_pad">
                <div class="col col-md-12 kenteken">
                  <h3>Zoek op kenteken</h3>
                  <form action="/kenteken/" method="get">
                    <input type="text" name="kentekenplaat" class="text-center kentekenplaat">
                        <button value="Zoek" type=submit class=search_button>Zoek</button>

                        @if($errors->any())
                        <h5>{{$errors->first()}}</h5>
                        @endif

                  </form>
                </div>
              </div>

              <div class="col col-md-6">

                    <form action="/filter" method="get">
                      <div class="col col-md-12">
                        <div class="form-group">Merk
                          <select name="make" class="form-control" id="selectmake">
                            <option value="Selecteer een auto">Selecteer een merk.</option>
                            @foreach($makes as $make)
                            <option value="{{$make['name']}}">{{$make['name']}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col col-md-10">
                        <div class="form-group">
                          <select name="model" class="form-control" id="selectmodel">
                            <option value="">Selecteer een model.</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-2">
                          <button type="submit" class="search_button">Filter</button>
                      </div>
                    </form>

                    <form autocomplete="off" class="form-group" onsubmit="window.location = '/categorie/sub/' + myInput.value; return false;">
                      <div class="col col-md-12">Zoek op onderdeel per naam</div>
                      <div class="col col-md-10">
                        <div class="form-group">
                        <input id="myInput" class="search-query form-control" placeholder="Onderdeel"" type="search" name="myCountry"></input>
                        </div>
                      </div>
                      <div class="col col-md-2">
                          <button type="submit" class="search_button">Zoek</button>
                      </div>
                    </form>
              </div>
  </div>

 <div class="tab-pane fade tab_layout" id="tab2">
              <div class="col col-md-1"></div>
              <div class="col col-md-4">Zoek op onderdeel- / artikelnummer</div>
              <form onsubmit="window.location = '/product/' + search.value; return false;">
                <div class="col col-md-5">
                  <input id="search" class="search-query form-control" placeholder="nummer"" type="search" name="search"></input>
                </div>
                  <div class="col col-md-2"><button type="submit" class="search_button">Zoek</button></div>

              </form>
 </div>


 <div class="tab-pane fade tab_layout" id="tab3">


              <div class="col col-md-1"></div>
              <div class="col col-md-4">Zoek op versnellingsbakcode</div>
              <form onsubmit="window.location = '/versnellingsbakcode/' + search.value; return false;"> 
                <div class="col col-md-5">
                  <input id="search" class="search-query form-control" placeholder="code"" type="search" name="search"></input>
                </div>
                  <div class="col col-md-2"><button type="submit" class="search_button">Zoek</button></div>
              </form>
 </div>
  
 <div class="tab-pane fade tab_layout" id="tab4">


              <div class="col col-md-1"></div>
              <div class="col col-md-4">Zoek op motorcode</div>
              <form onsubmit="window.location = '/motorcode/' + search.value; return false;"> 
                <div class="col col-md-5">
                  <input id="search" class="search-query form-control" placeholder="code"" type="search" name="search"></input>
                </div>
                  <div class="col col-md-2"><button type="submit" class="search_button">Zoek</button></div>
              </form>
 </div>

</div>
</div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="second_blok">
                            <h3><i class="fa fa-wrench"></i> Onderdelen</h3>
                            <p>Wij hebben meer dan 900 onderdelen op voorraad. Twijfel niet om onze voorraad online te bekijken, dit kan makkelijk en simpel via onze webshop.</p>
                           
                            <a class="blokhref" href="/onderdelen"><span class="blued">Bekijk</span> nu onze voorraad <i class="fa fa-arrow-right arrow-btn"></i> </a>

                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="second_blok">
                        <h3><i class="fa fa-ambulance"></i> Voertuigen voorraad</h3>
                        <p><a class="blokhref blockhrefone" href="/demontage_autos"><span class="blued">Demontage </span>voertuigen<i class="fa fa-arrow-right arrow-btn"></i> </a>
                        <a class="blokhref" href="/schade_autos"><span class="blued">Schade</span> voertuigen <i class="fa fa-arrow-right arrow-btn"></i> </a></p>
                        </div>
                    </div>
                </div>


              <div class="col-md-12 over_ons pc_versie">

                <div class="col-md-7">
                    <h2>Over ons</h2>
                    <p>Wij zijn gespecialiseerd in het verkoop van verschillende gebruikte auto-onderdelen & accessoires. Zowel particulieren als bedrijven kunnen bij Autodemontage Veenendaal terecht. Alle gebruikte onderdelen zijn terug te vinden op onze online voorraad. De onderdelen, die op onze online voorraad staan liggen allemaal ordelijk in een groot magazijn. Verder heeft Autodemontage Veenendaal ook een reparatie en ondershoudsbeurtservice. Wij kunnen onderdelen direct aan uw auto monteren en doen ook reparatie’s en onderhoudsbeurten op afspraak.</p>
                </div>
                <div class="col-md-5">
                    
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2455.9777215891536!2d5.5665190158455085!3d52.00728387972032!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c653c873c4ee8b%3A0xef37d53936e0ee6a!2sTurbinestraat+22A%2C+3903+LW+Veenendaal!5e0!3m2!1snl!2snl!4v1524430312037" width="100%" height="100%;" frameborder="0" allowfullscreen></iframe>
                </div>         

              </div>


              





            </div>
            </div>
          </div>
        
</div>
</div>

                <br />

    </section>
@endsection

@section('js')
     <script>

  $( "#selectmake" ).change(function() 
  {
    $.getJSON("/getmodel/"+ $(this).val() +"", function(jsonData){
        select = '<select name="position" class="form-control input-sm " required id="position" >';
          $.each(jsonData, function(i,data)
          {
               select +='<option value="'+data.name+'">'+data.name+'</option>';
           });
        select += '</select>';
        $("#selectmodel").html(select);
    });
  });
      </script>

      <script >
        function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
      });
}

/*An array containing all the country names in the world:*/
var countries = [""];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("myInput"), countries);
</script>

      </script>















      
<style>
  .footer_logos{
  max-width: 100% !important;

}

.footer_logo{
  width: 40px;
    float: left;
}

.footer_visa{
  width: 100px;
    float: left;
    margin-top: 5px;
}
</style>


@endsection

