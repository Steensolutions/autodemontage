@extends('template.app')

@section('content')

<section id="aanvraag">


<br />
<div class="container">
  	<div class="row">
        <div class="col-sm-2"></div>
  		<div class="col-sm-8  catagorie_blok black_border" style="padding:0px;">

            <div class="categorie_headtext all_color" style="width: 100%;">
                <h1> <i class="fa fa-wrench"></i> Een aanvraag doen</h1>
                <p> Is er een onderdeel wat u niet in onze webshop heeft kunnen vinden, of zoekt u ondersteuning van onze medewerkers. Vul dan astublieft het aanvraag formulier in. Wij zullen dan zo spoedig mogelijk contact opnemen.</p>
            </div>

                <br />

            <div class="aanvraag">
                

                @if(session('message'))
                <br />
                    <div class='alert alert-success'>
                        {{ session('message') }}
                    </div>
                @endif




                <form class="form-horizontal" method="POST" action="/aanvraag">
                        {{ csrf_field() }} 
                    
                    <div class="form-group">
                        <label for="kenteken">Kenteken: </label>
                        <input type="text" class="form-control" id="kenteken" placeholder="AB-12-CD" name="kenteken" required>
                    </div>

                    <div class="form-group">
                        <label for="merk">Automerk: </label>
                        <input type="text" class="form-control" id="merk" placeholder="Merk" name="merk" required>
                    </div>

                    <div class="form-group">
                        <label for="model">Automodel: </label>
                        <input type="text" class="form-control" id="model" placeholder="Model" name="model" required>
                    </div>

                    <div class="form-group">
                        <label for="Name">Naam: </label>
                        <input type="text" class="form-control" id="name" placeholder="Volledige (bedrijfs)naam" name="name" required>
                    </div>

                    <div class="form-group">
                        <label for="email">E-mail </label>
                        <input type="text" class="form-control" id="email" placeholder="Info@autodemontageveenendaal.nl" name="email" required>
                    </div>
                
                    <div class="form-group">
                        <label for="telefoonnummer">Telefoonnummer: </label>
                        <input type="text" class="form-control" id="telefoonnummer" placeholder="0318-307529" name="telefoonnummer" required>
                    </div>

                    <div class="form-group">
                        <label for="message">Bericht: </label>
                        <textarea type="text" class="form-control luna-message" id="message" placeholder="Typ hier uw bericht." name="message" rows="8" required></textarea>
                    </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" value="Send">Versturen</button>
                        </div>
                </form>
            </div>

        </div>
    </div>
</div>

</section>


@endsection
