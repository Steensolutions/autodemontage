@extends('template.app')

@section('content')
<section>


          <div class="container">
            <div class="row">

                @include('template.categoriebar')
                
                <div class="col-sm-9 padding-right">

                    <div class="row subcategorie_search">
                        <div class="col-md-1"></div>
                                     <form id="searchform">
                                    <div class="col col-md-8">
                                        <input type="text" class="search-query form-control" placeholder="Zoek" id="search-criteria" >
                                    </div>
                                    <div class="col col-md-3"><button type=submit id="search" class=search_button>Zoek</button></div>
                                        </form>
                    </div>
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">{{ $name }}</h2>
                        
                       
                            @foreach($cats as $cat) 
                            <div class="col-md-4 subcategorie_text">
                            <a href="/categorie/{{ $cat['categoryid'] }}?make={{$makename}}&model={{$modelname}}"><b>{{ $cat['categoryname'] }} </b> ({{ $cat['count'] }})</a>
                            </div>
                            @endforeach
                        
                    </div><!--features_items-->
                </div>
            </div>
        </div>
    </section>
@endsection
