@extends('template.app')

@section('content')

<div class="container">
 <div class="row">
                  <div class="col-sm-12">
                    <div class="test">
                        <div class="col-md-6">
                          <div class="second_blok">
                                <h3><i class="fa fa-ambulance"></i> Demontage auto's </h3>
                                <p> Wij hebben een breed assortiement aan gedemontage voertuigen.</p>
                                <p><a class="blokhref blockhrefone" href="/demontage_autos"><span class="blued">Bekijk hier </span>onze actuele verkoop voertuigen<i class="fa fa-arrow-right arrow-btn"></i> </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="second_blok">
                                <h3><i class="fa fa-wrench"></i> Schade auto's</h3>
                                <p> Wij hebben ook veel schade voertuigen.</p>
                                <b/>
                                <a class="blokhref" href="/schade_autos"><span class="blued">Bekijk hier </span> onze demontagevoertuigen voorraad <i class="fa fa-arrow-right arrow-btn"></i> </a>
                            </div>
                        </div>


                  </div>
                    </div>
                

                <br />

</div>
</p>
</div>


@endsection

@section('js')
@endsection