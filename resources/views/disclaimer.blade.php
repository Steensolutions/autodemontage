@extends('template.app')

@section('content')
<section>
        <div class="container">
            <div class="row">
                
                <div class="col-sm-9">
<div class="col-md-12">
                    <div class="top_blok">
                       <div class="top_blok_left col-sm-12">
                       <div id="content">

                  <!-- Content -->
                    <article>
                      <h2>Disclaimer</h2>
                      <p>

</p><p style="font-size: 15px; font-weight: bold;"> Autodemontage Veenendaal B.V. besteedt veel tijd en zorg aan de informatie die zij aanbiedt op haar website, maar geeft geen garantie ten aanzien van de juistheid van de verstrekte informatie. Bezoekers en/of gebruikers van de website kunnen op geen enkele wijze rechten ontlenen aan de op de website aangeboden informatie. Alle informatie is onder voorbehoud van prijswijzigingen, typefouten, vergissingen en marktontwikkelingen. De getoonde afbeeldingen zijn louter ter illustratie.</p>

De intellectuele eigendomsrechten op alle op de website aanwezige informatie zijn voorbehouden aan Autodemontage Veenendaal B.V. Het is niet toegestaan om informatie afkomstig van de website van Autodemontage Veenendaal B.V. zonder voorafgaande schriftelijke toestemming te kopiëren en/of te verspreiden in welke vorm dan ook. De website kan links bevatten naar websites die geen eigendom zijn van Autodemontage Veenendaal B.V.. Autodemontage Veenendaal B.V. is op geen enkele wijze verantwoordelijk voor de inhoud of het gebruik van deze website of voor de eventuele gevolgen van een bezoek aan één van deze websites.Door Autodemontage Veenendaal B.V. per e-mail verzonden informatie is uitsluitend bestemd voor de geadresseerde. Gebruik van deze informatie door anderen dan de geadresseerde is niet toegestaan.<br>
<p>
</p><p>Autodemontage Veenendaal B.V. staat niet in voor de juiste en volledige overbrenging van de inhoud van verzonden e-mails of voor tijdige ontvangst van de e-mails. Aan de u toegezonden informatie kan op geen enkele wijze rechten worden ontleend.</p>
<p>
Autodemontage Veenendaal B.V. respecteert de privacy van alle gebruikers van haar site en draagt er zorg voor dat de persoonlijke informatie die u ons verschaft vertrouwelijk wordt behandeld. Wij gebruiken uw gegevens om de bestellingen zo snel en gemakkelijk mogelijk te laten verlopen. Voor het overige zullen wij deze gegevens uitsluitend gebruiken met uw toestemming. Autodemontage Veenendaal B.V. zal uw persoonlijke gegevens niet aan derden verkopen en zal deze uitsluitend aan derden ter beschikking stellen die zijn betrokken bij het uitvoeren van uw bestelling.<br>
</p><p>
</p><p style="font-size: 15px; font-weight: bold;">Autodemontage Veenendaal B.V.<br>
Turbinestraat 22A<br>
3903LW Veenendaal</p>

                    </article>
<br />
                </div>
                           </div>
                       </div>

                   </div>
                   </div>
                </div>
    </section>
@endsection
