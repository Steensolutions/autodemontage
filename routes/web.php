<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', 'TaalController@show')->name('test');

Route::get('/categorie_blok', 'categoriecontroller@blok')->name('categorie_blok');

Route::get('/categorie/{categorie}', 'CategorieController@show');

Route::get('/categorie/sub/{subcategorie}', 'ProductController@index');

Route::get('/product/{product}', 'ProductController@show');

Route::get('/kenteken/{kenteken}', 'ProductController@kenteken');

Route::get('/kenteken/{kenteken}/{categorieid?}', 'CategorieController@showkenteken');

Route::get('/add-to-cart/{id}', 'CartController@addtocart');

Route::get('/remove-from-cart/{id}', 'CartController@removefromcart');

Route::get('/winkelwagen', 'CartController@index')->name('cart');

Route::get('/winkelwagen/leeg', 'CartController@clear')->name('leeg.cart');

Route::get('/checkout', 'PaymentController@index')->name('checkout');

Route::get('/kenteken', 'ProductController@kentekenget');

Route::post('/checkout/proceed', 'PaymentController@newPayment')->name('proceed');

Route::get('/betaald/{id}', 'PaymentController@betaald')->name('betaald');

Route::post('/paymentwebhook', 'PaymentController@webhook')->name('webhook');

Route::get('/reparatie-en-onderhoud', 'HomeController@reparatie')->name('reparatie');

Route::get('/onderdelen', 'HomeController@onderdelen')->name('onderdelen');






// Route::get('/contact', 'HomeController@contact')->name('contact');

Route::get('/verandertaal/{locale}', 'TaalController@setLanguage')->name('taalkeuze');

Route::get('/algemenevoorwaarden', function() {
	return view('algemenevoorwaarden');
});

Route::get('/privacy', function() {
	return view('privacy');
});

Route::get('/disclaimer	', function() {
	return view('disclaimer	');
});

Route::get('/account', 'AccountController@index')->name('account');

Route::get('/bestellingen', 'AccountController@bestellingen')->name('bestellingen');

Route::get('/bestelling/{id}', 'AccountController@bestelling')->name('bestelling');

Route::get('/gegevens', 'AccountController@gegevens')->name('gegevens');

Route::post('/gegevens/opslaan', 'AccountController@opslaan')->name('gegevensopslaan');

Route::get('/registreren', 'AccountController@register')->name('registeren');

// Accounts

Route::get('loguit', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/getmodel/{make}', 'HomeController@getModels')->name('getmodels');

Route::get('/filter', 'HomeController@filter')->name('filter');

Route::get('/contact', 'ContactController@show');
Route::post('/contact',  'ContactController@mailToAdmin')->name('sendcontact');

Route::get('/aanvraag', 'AanvraagController@show');
Route::post('/aanvraag',  'AanvraagController@mailToAanvraagAdmin')->name('aanvraag');

// Route::get('/aanvraag', 'HomeController@aanvraag')->name('aanvraag');
// Route::get('/aanvraag', 'AanvraagController@show');
// Route::post('/aanvraag',  'AanvraagController@mailToAdmin');


Route::post('/artikelnummer/{nummer}', 'HomeController@filternummer')->name('filternummer');

Route::get('/artikelnummer', 'HomeController@getfilternummer');

Route::get('/voorraad', 'VoorraadController@index');

Route::get('/demontage_autos/{nummer?}/{merkfilter?}', 'AutoController@index');

Route::get('/schade_autos', 'AutoController@schade_autos');



Route::get('/auto_zelf/{vehicleid}', 'AutoController@show')->name('showauto');

Route::post('/carfilter', 'AutoController@filter')->name('carfilter');


Route::get('/versnellingsbakcode/{gearboxid}' ,  'HomeController@versnellingsbakcode')->name('versnellingsbakcode');

Route::get('/motorcode/{motorid}' ,  'HomeController@motorcode')->name('motorcode');

Route::get('/test123', 'HomeController@test123')->name('test123');