<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type')->nullable();

            $table->string('aanhef')->nullable();

            $table->string('voornaam')->nullable();;

            $table->string('tussenvoegsel')->nullable();

            $table->string('achternaam')->nullable();;

            $table->string('telefoon')->nullable();;

            // Adresgegevens

            $table->string('postcode')->nullable();

            $table->string('huisnummer')->nullable();

            $table->string('toevoeging')->nullable();

            $table->string('straatnaam')->nullable();

            $table->string('plaatsnaam')->nullable();

            $table->string('land')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
