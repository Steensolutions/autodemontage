<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');

            // Betaling

            $table->string('order_id');

            $table->integer('user_id');

            $table->string('pid')->unique();

            $table->string('status');

            $table->string('prijs');

            // Persoonsgegevens

            $table->string('type');

            $table->string('aanhef')->nullable();

            $table->string('voornaam');

            $table->string('tussenvoegsel')->nullable();

            $table->string('achternaam');

            $table->string('email');

            $table->string('telefoon');

            // Adresgegevens

            $table->string('postcode');

            $table->string('huisnummer');

            $table->string('toevoeging')->nullable();

            $table->string('straatnaam');

            $table->string('plaatsnaam');

            $table->string('land')->nullable();

            $table->string('opmerkingen')->nullable();

            $table->string('bedrijfsnaam')->nullable();

            $table->string('btwnummer')->nullable();

            $table->string('kenteken')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
