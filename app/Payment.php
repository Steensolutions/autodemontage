<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

            protected $fillable = ['type', 'aanhef', 'voornaam', 'tussenvoegsel', 'achternaam', 'email', 'telefoon', 'postcode', 'huisnummer', 'toevoeging', 'straatnaam', 'plaatsnaam', 'land', 'bedrijfsnaam', 'btwnummer','opmerkingen', 'user_id', 'kenteken'];
    //
}
