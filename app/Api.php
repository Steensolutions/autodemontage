<?php

namespace App;

use SoapClient;

use Cart;

class Api
{
    
    function getCountries()
    {
        
        $client = new SoapClient('https://www.onderdelenlijn.nl/services/parts/v6.asmx?WSDL');
        
        $params = array(
            'credentials' => array(
                'username' => 'XML101238',
                'password' => 'K0rP5g0fVp'
            ),
            'message' => array(),
            'searchParameters' => array(
                'OnlyActive' => true
            )
        );
        
        $response = $client->cultures_get($params);
        
        $reportdata = json_decode(json_encode($response), true);
        
        return $reportdata;
    }
    
    function getData($type, $params)
    {
        
        $client = new SoapClient('https://www.onderdelenlijn.nl/services/parts/v6.asmx?WSDL');

        $params = array(
            'credentials' => array(
                'username' => 'XML101238',
                'password' => 'K0rP5g0fVp'
            ),
            'message' => array(),
            'searchParameters' => array(
                'OnlyActive' => true
            ),
            'parameters' => $params,
        );
        
        $response = $client->$type($params);
        
        $reportdata = json_decode(json_encode($response), true);
        
        return $reportdata;
        
    }

     function getCars($type, $params)
    {
        
        $client = new SoapClient('https://www.onderdelenlijn.nl/services/cars/v2.asmx?WSDL');
        
        $params = array(
            'credentials' => array(
                'username' => 'XML101238',
                'password' => 'K0rP5g0fVp'
            ),
            'message' => array(),
            'searchParameters' => array(
                'OnlyActive' => true
            ),
            'parameters' => $params,
        );
        
        $response = $client->$type($params);
        
        $reportdata = json_decode(json_encode($response), true);
        
        return $reportdata;
        
    }

    static function getImage($name) {

        $names = [
            'Motor en Toebehoren' => 'engine',
            'Computers en Elektronica' => 'battery',
            'Besturing' => 'steering-wheel',
            'Dashboard en Schakelaars' => 'indicators',
            'Interieur en Bekleding' => 'car-2',
            'Transmissie en Toebehoren' => 'gearshift',
            'Onderstel en Ophanging' => 'chassis',
            'Ruiten en Toebehoren' => 'window',
            'Uitlaatsysteem' => 'exhaust',
            'Verlichting' => 'car-lights',
            'Brandstofsysteem' => 'oil-1',
            'Airco en Verwarming' => 'frost-1',
            'Plaatwerk en Carrosserie' => 'car',
            'Sloten en Scharnieren' => 'car-key-1',
            'Accessoires' => 'parking-2',
            'Remmen' => 'break',
            'Banden en Velgen' => 'tire',
            'Accu en Toebehoren' => 'battery',
            'Leidingen en Rubbers' => 'suspension',
            'Overige Delen / Diversen' => 'mechanic',
        ];

       // dd($names);

        return $names[$name];

    }

    static function getId($id) {

        $ids = [
            '36' => 'Bouwjaar',
            '12' => 'Bouwjaar',
            '23' => 'Voertuigtype',
            '26' => 'Kilometerstand (km)',
            '121' => 'Kleppen',
            '17' => 'Motortype',
            '18' => 'Cilinderinhoud (cc)',
            '108' => 'Motorcode',
            '122' => 'Vermogen',
            '19' => 'Versnellingsbak type',
            '36' => 'Aandrijving',
            '22' => 'Deuren',
            '24' => 'Kleur voertuig',
            '128' => 'Schuifdak',
            '113' => 'Centrale deurvergrendeling',
            '134' => 'Wielbouten',
            '123' => 'Airco',
            '137' => 'Laknummer voertuig',
            '106' => 'ABS',
            '114' => 'Versnellingsbakcode',

            '14' => '14',
            '20' => '20',
            '27' => '27',
            '28' => '28',
            '101' => '101',
            '102' => '102',
            '110' => '110',
            '112' => '112',
            '131' => '131',
            '133' => '133',
            '135' => '135',
            '146' => '146',
            '149' => '149',



            
        ];

       // dd($names);

        return $ids[$id];

    }
        static function getProd($prod) {

        $prods = [
            '36' => 'Bouwjaar',
            '12' => 'Bouwjaar',
            '23' => 'Voertuigtype',
            '26' => 'Kilometerstand (km)',
            '121' => 'Kleppen',
            '17' => 'Motortype',
            '18' => 'Cilinderinhoud (cc)',
            '108' => 'Motorcode',
            '122' => 'Vermogen',
            '19' => 'Versnellingsbak type',
            '36' => 'Aandrijving',
            '22' => 'Deuren',
            '24' => 'Kleur voertuig',
            '128' => 'Schuifdak',
            '113' => 'Centrale deurvergrendeling',
            '134' => 'Wielbouten',
            '123' => 'Airco',
            '137' => 'Laknummer voertuig',
            '106' => 'ABS',
            '114' => 'Versnellingsbakcode',
            '29' => 'Materiaal',
            '25' => 'Merk onderdeel',
            '37' => 'Uitvoering',
            '35' => 'Kleur',
            '111' => 'Doorsnede (inches)',

            '14' => '14',
            '20' => '20',
            '27' => '27',
            '28' => '28',
            '101' => '101',
            '102' => '102',
            '110' => '110',
            '112' => '112',
            '131' => '131',
            '133' => '133',
            '135' => '135',
            '146' => '146',
            '149' => '149',
           
            

            
        ];

       // dd($names);

        return $prods[$prod];

    }

    public function getQty($id) {

        $qty = 0;

        foreach(Cart::content() as $cart) {

            if($cart->id === $id) {
                $qty = $cart->qty;
            }
            
        }

        return $qty;


    }
}