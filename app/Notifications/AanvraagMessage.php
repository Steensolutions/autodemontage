<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Http\Requests\AanvraagFormRequest;

class AanvraagMessage extends Notification
{
    use Queueable;

   

    protected $message;
    public function __construct(AanvraagFormRequest $message)
    {
        $this->message = $message;
    }


    public function via($notifiable)
    {
        return ['mail'];
    }   


    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(config('admin.name') . ", Onderdelen formulier")
                    ->greeting("Bericht via aanvraag formulier")
                    ->salutation($this->message->name)
                    ->line($this->message->message)
                    ->line($this->message->telefoonnummer)
                    ->line($this->message->merk)
                    ->line($this->message->model)
                    ->line($this->message->kenteken);
    }




    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
