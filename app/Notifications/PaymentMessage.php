<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PaymentMessage extends Notification
{
    use Queueable;

   

    protected $order;
    protected $orderinfos;

    public function __construct($order, $orderinfos)
    {
        $this->order = $order;
        $this->orderinfos = $orderinfos;


        // dd($PaymentMessage);
    }


    public function via($notifiable)
    {
        return ['mail'];
    }   


    public function toMail($notifiable)
    {


        $orderinfos = $this->orderinfos;

        $info = '';

        foreach($orderinfos as $orderinfo){

    $info .=  "Besteling-informatie:  "
              .$orderinfo->naam.",   "
              .$orderinfo->voertuig.",   "
              ."€"
              .$orderinfo->prijs.
              "      ";

          }

        return (new MailMessage)
                    ->subject(config('admin.name') . ", Payment formulier")

                    ->from($this->order->email, $this->order->name)
                    ->greeting("Nieuwe bestelling")
                    ->line($this->order->voornaam . " " . $this->order->tussenvoegsel . " " . $this->order->achternaam)
                    ->line($this->order->bedrijfsnaam . ", " . $this->order->btwnummer)
                    ->line($this->order->telefoon . ", " . $this->order->email)
                    ->line($this->order->straatnaam . ", " . $this->order->huisnummer . ", " . $this->order->toevoeging)
                    ->line($this->order->postcode . ", " . $this->order->plaatsnaam .", ". $this->order->land)
                    ->line($this->order->updated_at)
                    ->line($this->order->kenteken)
                    ->line($this->order->opmerkingen)
                    ->line($info)
                    ->line("totaal: €" . $this->order->prijs)
                    ->line("http://www.autodemontageveenendaal.nl/betaald/" . $this->order->pid);
    }




    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
