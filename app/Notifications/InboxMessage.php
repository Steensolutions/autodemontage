<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Http\Requests\ContactFormRequest;

use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Mail\Mailer;

use Illuminate\Http\Request;
class InboxMessage extends Notification
{
  


protected $message;
    public function __construct(contactFormRequest $message)
    {
        $this->message = $message;
    }



    public function via($notifiable)
    {
        return ['mail'];
    }




    public function toMail($notifiable)

{


    
        return (new MailMessage)
                    ->subject(config('admin.name') . ", Contact formulier")
                    ->greeting("Bericht via contact formulier")
                    ->salutation($this->message->name)
                    ->from($this->message->email, $this->message->name)
                    ->line($this->message->message)
                    ->line($this->message->telefoonnummer);
    }




    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
