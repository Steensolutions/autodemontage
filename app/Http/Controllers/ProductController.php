<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;
use Cart;
use App;
use Input;
use Redirect;

class ProductController extends Controller
{
    public function index($subcategorie, $makename = null, $modelname = null) {

                $filtermake = false;
        $filtermodel = false;
        if(!empty($_GET['make'])) {
            $makename = $_GET['make'];
            $filtermake = true;
        } else {
            $makename = '';
        }
        if(!empty($_GET['model'])) {
            $modelname = $_GET['model'];
            $filtermodel = true;
        } else {
            $modelname = ""; 
        }

    	// Tonen van een lijst met producten
    	$api = new Api();
        $type = 'stock_rows_get';
        $params = array(
                'culture' => App::getLocale(),
                'modeltypeid' => '0',
                'vehicleid' => '0',
                'group' => 'none',
                'partname' => $subcategorie,
                'instock' => true,
                'makename' => $makename,
                'modelname' => $modelname,
                'showsold' => false,
                'showsynonyms' => false,
                'showcount' => true,
                'direction' => 'ascending',
                'sort' => array(
                	'direction' => 'ascending'
                ),
                'pageindex' => '1',
                'pagesize' => '20',
                'showcount' => true,
                'showsuppliers' => false,
                'showtvi' => true,
                
            );
        $reportdata = $api->getData($type, $params);

        //dd($reportdata);
         $products = $reportdata['stock_rows_getResult']['result']['rows'];

        
        // dd($products);




        if(empty($products))
        
            return redirect()->back()->with('message', 'Onderdeel niet gevonden');
    
        else


        if(count($products['stockrow']) == 21) {
          $products = $reportdata['stock_rows_getResult']['result']['rows'];
    
        } else {
          $products = $reportdata['stock_rows_getResult']['result']['rows']['stockrow'];

        }

        $name = $reportdata['stock_rows_getResult']['input']['partname'];

       // dd($products['media']['images']['root']);
  

       

        return view('products')->with(compact('products', 'name', 'makename', 'modelname'));

    }

    public function show($id) {


    	$api = new Api();

        $type = 'stock_row_get';
        $params = array(
                'culture' => App::getLocale(),
                'partid' => $id,
            );
        $reportdata = $api->getData($type, $params);
        // dd($reportdata);
        $products = $reportdata['stock_row_getResult'];

        if($products['result']['row']['partid'] == 0)

            return redirect()->back()->with('message', 'Product niet gevonden');
        else

            $gegevens = $products['result']['row']['properties']['partpropertyvalue']; 
            
        	return view('product')->with(compact('products', 'gegevens', 'prod' , 'api'));
    }

        public function kenteken($kenteken) {

    	$api = new Api();
        $type = 'tvi_get';
        $params = array(
                'culture' => App::getLocale(),
                'licenseplate' => $kenteken,
            );
        $reportdata = $api->getData($type, $params);

        if(empty($reportdata['tvi_getResult']['result']['basic']['automatemakemodel']['modelname'])) {
            $message = "Dit kenteken bestaat niet.";
            return redirect()->back()->withErrors(['We kunnen geen auto vinden met dit kenteken.']);
        }

        $modelname = $reportdata['tvi_getResult']['result']['basic']['automatemakemodel']['modelname'];
        $makename = $reportdata['tvi_getResult']['result']['basic']['automatemakemodel']['makename'];
        $type1 = 'stock_part_categories_get';
        $params1 = array(
                'culture' => App::getLocale(),
                'categoryid' => '0',
                'makename' => $makename,
                'modelname' => $modelname,
            );
        $data = $api->getData($type1, $params1);
                if(empty($data['stock_part_categories_getResult']['result']['categories']['category'])) {
            $message = "Wij hebben geen onderdeel van dit model in de verkoop.";
            return redirect()->back()->withErrors(['Deze auto bestaat niet.']);
        }
        $cats = $data['stock_part_categories_getResult']['result']['categories']['category'];
        $name = 'Categoriën voor deze '.$makename.' '.$modelname.'';

    	return view('hcato')->with(compact('cats', 'name', 'parts', 'makename', 'modelname'));
    }

    public function kentekenget(Request $request) {
    	$kenteken = $request->input('kentekenplaat');
    	return Redirect::to('/kenteken/'.$kenteken.'');
    }
}
