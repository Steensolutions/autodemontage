<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Payment;

use App\Api;
use App;
use Mollie;
use Cart;
use Illuminate\Support\Facades\Input;
use Log;
use DB;
use Auth;
use App\Orderinfo;

use App\PaymentAdmin;
use App\Notifications\PaymentMessage;
use App\Http\Requests\PaymentFormRequest;
use App\Http\Controllers\Controller;

use App\BestellingAdmin;
use App\Notifications\BestellingMessage;
use App\Http\Requests\BestellingFormRequest;

use Mail;
use Illuminate\Mail\Mailer;




class PaymentController extends Controller
{
	public function betaald($pid) {


		// if ($order->status === "1"){

		$order = Payment::where('pid', $pid)->first();

		// dd($order);

		$orderinfos = Orderinfo::where('order_id', $order->order_id)->get();

		// dd($orderinfos);


		return view('/betaald')->with(compact('order' , 'orderinfos', 'products', 'gegevens'));
		}

		// else{
		// 	return 'error';
		// }


	public function webhook(Request $request, PaymentAdmin $PaymentAdmin, BestellingAdmin $BestellingAdmin ) {

		$id = $request->id;

		$payment = Mollie::api()->payments()->get($id);

		$order = Payment::where('order_id', $id)->first();

		$orderinfos = Orderinfo::where('order_id', $id)->get();


		if ($payment->isPaid())
		{
			
		
		$PaymentAdmin->notify(new PaymentMessage($order, $orderinfos));
		
		$data =  array( 'order' => '$order',
						'orderinfos' => '$orderinfos');

		Mail::send('mail_klant', $data, function($message, $order, $data)
        {
        	$data =  array( 'order' => '$order',
						'orderinfos' => '$orderinfos');
        	$message->to($order->mail , $order->pid)->subject('Bedankt voor uw bestelling');
        });


		DB::update('update payments set status = 1 where order_id = ?', [$id]);


		    return 'Betaling is goed gegaan';


		} else {

			return 'Betaling is fout gegaan';
			// Niks
		}
	}


	public function index() {

		$content = Cart::content();
    	$price = Cart::total();

		return view('checkout')->with(compact('content', 'price'));
	}

	public function receivedPayment() {

	}


	public function newPayment(Request $request) {

		$validatedData = $request->validate([
        'type' => 'required|string',
        'aanhef' => '',
        'voornaam' => 'required|string',
        'tussenvoegsel' => '',
        'achternaam' => 'required|string',
        'email' => 'required',
        'telefoon' => 'required',
        'postcode' => 'required|size:6',
        'huisnummer' => 'required',
        'straatnaam' => 'required|string',
        'plaatsnaam' => 'required|string',
        'land' => 'required|string',
        'kenteken' => '',
        'BTWnummer' => '',
        'bedrijfsnaam' => '', 

        ]);

        $order = new Payment($request->all());

        $pid = uniqid();

        $order->pid = $pid;

        // dd($order->pid);

		$betaling = Mollie::api()->payments()->create([
			"amount" => Cart::total(),
			"description" => "First API Payment",
			"redirectUrl" => url('/betaald/'.$pid.''), 
			"webhookUrl" => url('/paymentwebhook')
		]);

		$order->prijs = Cart::total();

		$betalings = Mollie::api()->payments()->get($betaling->id);




		$order->order_id = $betalings->id;

		if(Auth::user()) {
			$order->user_id = Auth::user()->id;
		} else {
			$order->user_id = $user_id = '0';
		}

		$order->status = 0;

		$order->save();

		$link = $betalings->links->paymentUrl;


		$content = Cart::content();

		foreach($content as $content) {
			$product = new Orderinfo();
			$product->prijs = $content->price;
			$product->order_id = $betalings->id;
			if(Auth::user()) {
				$product->user_id = Auth::user()->id;
			} else {
				$product->user_id = 0;
			}
			$product->naam = $content->name;
			$product->prijs = $content->price;
			$product->voertuig = $content->options->car;
			$product->image = $content->options->image;
			$product->id = $content->id;

			$product->save();

		}

        return redirect()->away($link);


	}
    
}
