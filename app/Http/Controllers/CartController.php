<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App;
use App\Api;

class CartController extends Controller
{
    public function index() {
    	$content = Cart::content();
    	$price = Cart::total();


        // dd($content);
    	return view('winkelwagen')->with(compact('content', 'price'));
    }

       public function addtocart($id) { 

     	$api = new Api();
        $type = 'stock_row_get';
        $params = array(
                'culture' => App::getLocale(),
                'partid' => $id,
            );
        $reportdata = $api->getData($type, $params);
        //dd($reportdata);
        $products = $reportdata['stock_row_getResult'];
        $name = $products['result']['row']['partname'];
        $price = $products['result']['row']['properties']['partpropertyvalue'][0]['values']['string'];

    	$car = ''.$products['result']['row']['makename'].' '.$products['result']['row']['modelname'].'';

        $image = $products['result']['row']['media']['images']['root'];
    	Cart::add($id, $name, 1, str_replace(',', '.', $price), compact('car', 'image'));
    	$message = "Succesvol toegevoegd aan winkelwagen.";

    	return redirect('/winkelwagen');
    	
    }

    public function clear() {
        Cart::destroy();
        return redirect('/winkelwagen');
    }

    public function removefromcart($id) {
    	Cart::remove($id);
    	return redirect('/winkelwagen');
    }
}
