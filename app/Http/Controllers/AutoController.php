<?php

namespace App\Http\Controllers;

use App;

use App\Api;

use Illuminate\Support\Facades\Response;

use Illuminate\Http\Request;

class AutoController extends Controller
{


    public function index($amount = 1, $merkfilter = '')
    {

      $api = new Api();
        $type = 'stock_rows_get';
        $params = array(
                'culture' => App::getLocale(),
                'pageindex' => $amount,
                'pagesize' => 20,
                'sort' => array(
                	'direction' => 'ascending'
                ),
                'showcount' => true,
                'showsuppliers' => true,
                'showtvi' => true,
                'makename' => $merkfilter,


            );
            $reportdata = $api->getCars($type, $params);
            
            // dd($reportdata);

            $makes = $reportdata['stock_rows_getResult']['result']['rows']['stockrow'];

            // dd($makes);


        $type_merken = 'makes_get';
        $params_merken = array(
                'culture' => App::getLocale(),
 
            );
            $reportdata = $api->getCars($type_merken, $params_merken);
            $categoriesfilter = $reportdata['makes_getResult']['result']['list']['make'];

            // dd($categoriesfilter);
            
            $amount_cars = $reportdata['makes_getResult']['result']['itemcount'];

            $amount_pages = $amount_cars / 20;

           


        return view('demontage_autos')->with(compact('makes' , 'amount' , 'reportdata', 'amount_cars' , 'categoriesfilter', 'model'));
    }

public function schade_autos($amount = 1)
    {

      $api = new Api();


        $type = 'stock_rows_get';      
        $params = array(
                'culture' => App::getLocale(),
                'pageindex' => $amount,
                'pagesize' =>  150,
                'sort' => array(
                    'direction' => 'ascending'
                ),
                'showcount' => true,
                'showsuppliers' => true,
                'showtvi' => true,
            );


            $reportdata = $api->getCars($type, $params);
           
            // dd($reportdata);

            $makes = $reportdata['stock_rows_getResult']['result']['rows']['stockrow'];
            


        return view('schade_autos')->with(compact('makes', 'amount', 'amount_tot'));
    }






    public function show($vehicleid , $amount = 1)
    {

     $api = new Api();
        $type = 'stock_row_get';
        $params = array(
                'culture' => App::getLocale(),
                'vehicleid' => $vehicleid,

            );
        $reportdata = $api->getCars($type, $params);

            // dd($reportdata);
       
        $makes = $reportdata['stock_row_getResult']['result']['row'];

        // dd($makes);

        $gegevens = $reportdata['stock_row_getResult']['result']['row']['properties']['propertyvalue'];
        

        $type1 = 'stock_rows_get';
        $params1 = array(
        	'culture' => App::getLocale(),
                'pageindex' => $amount,
                'pagesize' => 10,
                'sort' => array(
                	'direction' => 'ascending'
                ),
                'showcount' => true,
                'showsuppliers' => true,
                'showtvi' => true,


            );
		$reportdata = $api->getCars($type1, $params1);

        $caroverzicht = $reportdata['stock_rows_getResult']['result']['rows']['stockrow'];



         return view('auto_zelf')->with(compact('id', 'makes', 'caroverzicht' , 'gegevens', 'naam', 'api'));
    
    }


}
