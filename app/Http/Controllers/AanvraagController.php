<?php

Namespace App\Http\Controllers;

use App\Notifications\AanvraagMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\AanvraagFormRequest;
use App\AanvraagAdmin;
use Illuminate\support\facades\Mail;



use App\PaymentAdmin;
use App\Notifications\PaymentMessage;
use App\Http\Requests\PaymentFormRequest;

Class AanvraagController extends Controller
{
	public function show() 
	{
		return view('aanvraag');
	}

	public function mailToAanvraagAdmin(AanvraagFormRequest $message, AanvraagAdmin $admin)
	{        //send the admin an notification
		$admin->notify(new AanvraagMessage($message));



		// redirect the user back
		return redirect()->back()->with('message', 'Wij helpen u zo spoedig mogelijk!');
	}
}