<?php

namespace App\Http\Controllers;


use App;

use App\Api;

use Illuminate\Support\Facades\Response;

use Illuminate\Http\Request;

use Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    //    $this->middleware('auth');
    }

     public function artikelnummer(Request $request) {
        $nummer = $request->input('artikelnummer');
        return Redirect::to('/artikelnummer/'.$nummer.'');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Makes opvragen uit API
        $api = new Api();
        $type = 'stock_makes_get';
        $params = array(
                'culture' => App::getLocale()
            );
        $reportdata = $api->getData($type, $params);
        $makes = $reportdata['stock_makes_getResult']['result']['makes']['make'];

       // dd($makes);

        return view('home')->with(compact('makes'));
    }

    public function getModels($make) {
        if (Request()->ajax())
       {
                    $api = new Api();
                    $type = 'stock_models_get';
                    $params = array(
                'culture' => App::getLocale(),
                'makename' => $make,
                'showsmodels' => 1,
                'instock' => 1
            );
            $reportdata = $api->getData($type, $params);
            $models = $reportdata['stock_models_getResult']['result']['models']['model'];

    

           return Response::json( $models );
      }
    }


    public function filter() {
        $make = $_GET['make'];
        $model = $_GET['model'];

        if($model === "undefined"){
            $model = '';
        }


        $api = new Api();

        $reportdata = $api->getData('stock_part_categories_get', array(
                'culture' => App::getLocale(),
                'categoryid' => 0,
                'makename' => $make,
                'modelname' => $model,
            ));


        $categoriesfilter = $reportdata['stock_part_categories_getResult']['result']['categories']['category'];

        // dd($categoriesfilter);

        return view('filteronderdelen')->with(compact('categoriesfilter'));

    }

    public function partfilter() {
        $make = $_GET['make'];
        $model = $_GET['model'];

        $api = new Api();

        $reportdata = $api->getData('stock_part_categories_get', array(
                'culture' => App::getLocale(),
                'categoryid' => 0,
                'makename' => $make,
                'modelname' => $model,
            ));
        $categoriesfilter = $reportdata['stock_part_categories_getResult']['result']['categories']['category'];
        $parts = $categoriesfilter['parts']['categorypart'];


        // dd($categoriesfilter);

        return view('filteronderdelen')->with(compact('categoriesfilter', 'parts'));

    }

    public function filternummer($nummer) {
        $api = new Api();
        $type = 'stock_row_get';
        $params = array(
                'culture' => App::getLocale(),
                'partid' => $nummer,
            );
        $reportdata = $api->getData($type, $params);
        //dd($reportdata);
        $products = $reportdata['stock_row_getResult'];

        // dd($products);

        return view('product')->with(compact('products'));

        if(empty($reportdata['tvi_getResult']['result']['basic']['automatemakemodel']['modelname'])) {
            $message = "Dit kenteken bestaat niet.";
            return redirect()->back()->withErrors(['We kunnen geen auto vinden met dit kenteken.']);
        }
    }

    public function reparatie() 
    {
        return view('reparatie');
    }

    public function welcome()
    {
        return view('welcome');
    }

    public function onderdelen()
    {

      $api = new Api();
        $type = 'stock_makes_get';
        $params = array(
                'culture' => App::getLocale()
            );
        $reportdata = $api->getData($type, $params);
        $makes = $reportdata['stock_makes_getResult']['result']['makes']['make'];

       // dd($makes);

        return view('onderdelen')->with(compact('makes'));



    }

    public function contact()
    {
      return view('contact');
    }


    public function aanvraag()
    {
      return view('aanvraag');
    }    

    public function versnellingsbakcode($gearboxid = 1 , $product = 1)
    {
        $api = new Api();
        $type = 'stock_rows_get';
        $params = array(
            'culture' => App::getLocale(),
            'modeltypeid' => 0,
            'vehicleid' => 0,
            'group' => 'none',
            'code' => array(
                'type'=> 'none'),
            'sort' => array(
                'direction' => 'ascending'),
            'pageindex' => 1,
            'pagesize' => 50,
            'showcount' => true,
            'showsuppliers' => true,
            'showtvi' => true,
            'partname' => 'Versnellingsbak',
            'filters' => array(
                'partpropertyfilter' => array(
                    'propertyid' => 114,
                    'condition' => 'equals',
                    'values' => array(
                        'string' => $gearboxid
                    ),
                ),
            ),
            );

        $reportdata = $api->getData($type, $params);

        if(empty($reportdata['stock_rows_getResult']
        ['result']['rows']['stockrow']))
        {
            return Redirect::to('/categorie/sub/Versnellingsbak')->with('message', 'Product niet gevonden');
        } else
        {

            $product = $reportdata['stock_rows_getResult']
            ['result']['rows']['stockrow']['partid']
            ;
        

       // dd($product);
            return Redirect::to('/product/'.$product.'');
        }
    }

    public function motorcode($motorid = 1 , $product = 1)
    {
        $api = new Api();
        $type = 'stock_rows_get';
        $params = array(
            'culture' => App::getLocale(),
            'modeltypeid' => 0,
            'vehicleid' => 0,
            'group' => 'none',
            'code' => array(
                'type'=> 'none'),
            'sort' => array(
                'direction' => 'ascending'),
            'pageindex' => 1,
            'pagesize' => 50,
            'showcount' => true,
            'showsuppliers' => true,
            'showtvi' => true,
            'partname' => 'Motor',
            'filters' => array(
                'partpropertyfilter' => array(
                    'propertyid' => 27,
                    'condition' => 'equals',
                    'values' => array(
                        'string' => $motorid
                    ),
                ),
            ),
            );

        $reportdata = $api->getData($type, $params);

        if(empty($reportdata['stock_rows_getResult']
        ['result']['rows']['stockrow']))
        {
            return Redirect::to('/categorie/sub/Motor')->with('message', 'Product niet gevonden');;
        } else
        {
            $product = $reportdata['stock_rows_getResult']
            ['result']['rows']['stockrow']['partid']
            ;      
           // dd($product);
            return Redirect::to('/product/'.$product.'');
        }

    }

        public function test123($make = null, $model = null)
    {
        // $make = $_GET['make'];
        // $model = $_GET['model'];

        $api = new Api();

        $reportdata = $api->getData('stock_part_categories_get', array(
                'culture' => App::getLocale(),
                'categoryid' => 0,
                'makename' => $make,
                'modelname' => $model,
            ));
        $categoriesfilter = $reportdata['stock_part_categories_getResult']['result']['categories']['category'];
        $parts = $categoriesfilter[0]['parts']['categorypart'];



            return view('/test123')->with(compact('parts'));

    }

}
