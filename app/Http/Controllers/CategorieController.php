<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Api;

use App;

class CategorieController extends Controller
{
    

    public function show($categorie) {
        $filtermake = false;
        $filtermodel = false;
        if(!empty($_GET['make'])) {
            $makename = $_GET['make'];
            $filtermake = true;
        } else {
            $makename = '';
        }
        if(!empty($_GET['model'])) {
            $modelname = $_GET['model'];
            $filtermodel = true;
        } else {
            $modelname = ""; 
        }
    	$api = new Api();
        $type = 'stock_part_categories_get';
        $params = array(
                'culture' => App::getLocale(),
                'categoryid' => $categorie,
                'makename' => $makename,
                'modelname' => $modelname
            );
        $reportdata = $api->getData($type, $params);
        $cats = $reportdata['stock_part_categories_getResult']['result']['categories']['category']['parts']['categorypart'];
        $name = $reportdata['stock_part_categories_getResult']['result']['categories']['category']['categoryname'];
        //67($name);
        return view('categorie')->with(compact('cats', 'name', 'makename', 'modelname', 'filtermodel', 'filtermake'));
    }


    public function showkenteken($kenteken, $categorie) {
    	$api = new Api();
        $type1 = 'tvi_get';
        $params1 = array(
                'culture' => App::getLocale(),
                'licenseplate' => $kenteken,
            );
        $model = $api->getData($type1, $params1);

        $modelname = $model['tvi_getResult']['result']['basic']['automatemakemodel']['modelname'];
        $makename = $model['tvi_getResult']['result']['basic']['automatemakemodel']['makename'];
        $type = 'stock_part_categories_get';
        $params = array(
                'culture' => App::getLocale(),
                'categoryid' => $categorie,
                'modelmake' => $modelname,
                'makename' => $makename,  
            );
        $reportdata = $api->getData($type, $params);
        $cats = $reportdata['stock_part_categories_getResult']['result']['categories']['category']['parts']['categorypart'];
        $name = $reportdata['stock_part_categories_getResult']['result']['categories']['category']['categoryname'];
        //dd($reportdata);
        return view('hcatosub')->with(compact('cats', 'name', 'makename', 'modelname'));
    }
}
