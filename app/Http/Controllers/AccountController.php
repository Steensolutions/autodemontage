<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Payment;
use Cart;
use App\Orderinfo;
use App\User;



class AccountController extends Controller
{
	public function index() {

		if(Auth::user()) {

		$criteria = ['user_id' => Auth::user()->id, 'status' => '1'];

		$bestellingen = Payment::where($criteria)->get();

	}

		return view('account.account')->with(compact('bestellingen'));
	}

		public function bestellingen() {

		if(Auth::user()) {

		$criteria = ['user_id' => Auth::user()->id, 'status' => '1'];

		$bestellingen = Payment::where($criteria)->get();

	}

		return view('account.bestellingen')->with(compact('bestellingen'));
	}

	public function bestelling($id) {
		$criteria = ['user_id' => Auth::user()->id, 'order_id' => $id];
		$bestelling = Payment::where($criteria)->first();

		$criteria2 = ['user_id' => Auth::user()->id, 'order_id' => $id];

		$products = Orderinfo::where($criteria2)->get();

		$price = 0;

		foreach($products as $product) {
			$price = $price + $product->prijs;
		}

		return view('account.bestelling')->with(compact('bestelling', 'products', 'price'));
	}

	public function gegevens() {

		return view('account.gegevens');
	}

	public function opslaan(Request $request) {
		$user = User::where('id', Auth::user()->id)->firstOrFail();

		$user->fill(\Input::all());

		$user->type = $request->type;

		$user->save();

		return view('account.gegevens');

	}

	public function register() {

		return view('account.register');
	}
    //
}
