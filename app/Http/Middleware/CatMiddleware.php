<?php

namespace App\Http\Middleware;

use Closure;
use App\Api;
use View;

use Session;

use App;

class CatMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api = new Api();
        $reportdata = $api->getCountries();
        $countries = $reportdata['cultures_getResult']['result']['list']['culturesgetitem'];
        View::share('countries', $countries);

        $reportdata = $api->getData('stock_part_categories_get', array(
                'culture' => App::getLocale(),
                'categoryid' => 0
            ));
        $cats = $reportdata['stock_part_categories_getResult']['result']['categories']['category'];
        View::share('categories', $cats);
        return $next($request);
    }
}
