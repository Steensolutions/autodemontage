<?php

namespace App\Http\Middleware;

use Closure;

use Session;

use App;

class CheckTaal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
public function handle($request, Closure $next)
{

    if(empty(Session::get('language'))) {
         Session::put('language', 'nl');
    }

    $taal = Session::get('language');

        App::setLocale($taal);

    return $next($request);
}
}
